############################################################################
# Copyright 2018 Bridgette Tool Kit Open Source Contributors               #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#     http://www.apache.org/licenses/LICENSE-2.0                           #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
############################################################################

# File name: CMakeLists.txt
#
# Description: Bridgette Tool Kit root CMakeLists.txt. Contains project
#              configuration that is common for all components.

cmake_minimum_required (VERSION 3.3 FATAL_ERROR)

project (Bridgette
  LANGUAGES CXX
  )


##################
# CMake settings #
##################
set (CMAKE_MODULE_PATH
  ${CMAKE_MODULE_PATH}
  ${PROJECT_SOURCE_DIR}/cmake/modules
  )

include (utk.cmake/utk_cmake_build_and_install_options)
include (utk.cmake/utk_cmake_install)
include (utk.cmake/utk_cmake_package)
include (utk.cmake/utk_cmake_product)
include (utk.cmake/utk_cmake_target)


set_property (GLOBAL PROPERTY USE_FOLDERS true)

set (CMAKE_COLOR_MAKEFILE ON)


# Boost settings
if (WIN32)
  # According to documantation:
  #
  # On Visual Studio and Borland compilers Boost headers request automatic
  # linking to corresponding libraries. This requires matching libraries to be
  # linked explicitly or available in the link library search path. In this case
  # setting Boost_USE_STATIC_LIBS to OFF may not achieve dynamic linking. Boost
  # automatic linking typically requests static libraries with a few exceptions
  # (such as Boost.Python).
  #
  # That's why for static libraries it is required to explicitly enable static
  # libraries usage.
  set (Boost_USE_STATIC_LIBS ON)
endif (WIN32)

#####################
# Bridgette project #
#####################
set (${PROJECT_NAME}_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}")


add_subdirectory (Chai3D)

add_subdirectory (Qt)


if (${PROJECT_NAME}_ENABLE_TESTING)
  enable_testing ()
endif ()
