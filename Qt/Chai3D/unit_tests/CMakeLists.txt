# Copyright 2018 Bridgette Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# File name: Bridgette/Qt/Chai3D/unit_tests/CMakeLists.txt
#
# Description: Bridgette Qt-Chai3D Integration Library test suite root

include (utk.cmake/utk_cmake_package)


set (INSTALL_TESTS          ${${PROJECT_NAME}_INSTALL_TESTS})
set (PARENT_PROJECT         ${PROJECT_NAME})
set (PARENT_PROJECT_FOLDER  ${PROJECT_FOLDER})


utk_cmake_download_and_use_googletest (
  OUTPUT_ADDITIONAL_TARGET_PROPERTIES ADDITIONAL_TEST_PROPERTIES
  )


set (ORIGINAL_CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH})
set (CMAKE_PREFIX_PATH
  "${CMAKE_PREFIX_PATH}, ${PROJECT_BINARY_DIR}/BridgetteQt"
  )


find_package (BridgetteQtChai3D REQUIRED)


add_subdirectory (v1)


set (CMAKE_PREFIX_PATH ${ORIGINAL_CMAKE_PREFIX_PATH})
unset (ORIGINAL_CMAKE_PREFIX_PATH)
