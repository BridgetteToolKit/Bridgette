// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Chai3D/BridgetteQtChai3D/include/Bridgette/Qt/Chai3D/v1/OpenGlRendererImpl.hpp
//
// Description: An OpenGL renderer class integrating Chai3D 3D rendering in Qt.
//              PIMPL class declaration.


#ifndef QT_CHAI3D_BRIDGETTEQTCHAI3D_SRC_BRIDGETTE_QT_CHAI3D_V1_OPENGLRENDERERIMPL_HPP
#define QT_CHAI3D_BRIDGETTEQTCHAI3D_SRC_BRIDGETTE_QT_CHAI3D_V1_OPENGLRENDERERIMPL_HPP


#include "Bridgette/Qt/Chai3D/BridgetteQtChai3D_export.h"

#include "Bridgette/Qt/Chai3D/v1/OpenGlRenderer.hpp"


#include <QSize>

#include <timers/CFrequencyCounter.h>


BRIDGETTE_QT_CHAI3D_V1_OPEN_NAMESPACE


class BRIDGETTEQTCHAI3D_NO_EXPORT OpenGlRenderer::Impl {
public:
	Impl ();


	QSize viewport_size;

	std::unique_ptr< ::chai3d::cFrequencyCounter > frame_rate_counter;

	std::weak_ptr< ::chai3d::cCamera > camera;
	utk::meta::type_erasure::any_lockable_weak_ptr render_lock;

	::chai3d::cEyeMode eye_mode = ::chai3d::C_STEREO_LEFT_EYE;
};


BRIDGETTE_QT_CHAI3D_V1_CLOSE_NAMESPACE


#endif // QT_CHAI3D_BRIDGETTEQTCHAI3D_SRC_BRIDGETTE_QT_CHAI3D_V1_OPENGLRENDERERIMPL_HPP
