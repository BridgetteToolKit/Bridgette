// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Chai3D/BridgetteQtChai3D/include/Bridgette/Qt/Chai3D/v1/SphericalCamera.cpp
//
// Description: The class for controlling the Chai3D camera in spherical
//              coordinates. Interface class implementation.


#include "SphericalCameraImpl.hpp"
#include <utk/idiom/Pimpl_impl.hpp>


#include "Bridgette/Qt/Chai3D/v1/Chai3dOpenGlVectorConversion.hpp"


BRIDGETTE_QT_CHAI3D_V1_OPEN_NAMESPACE


SphericalCamera::SphericalCamera (cCameraSharedPtr i_camera)
    : impl_{std::move (i_camera)} {
}


SphericalCamera::~SphericalCamera () = default;


::chai3d::cCamera* SphericalCamera::Camera () const noexcept {
	return impl_->camera.get ();
}


void SphericalCamera::SetCamera (cCameraSharedPtr i_camera) {
	impl_->camera = std::move (i_camera);
}


double SphericalCamera::FieldOfViewAngleImpl () const noexcept {
	if (!impl_->camera) {
		return 0.0;
	}

	return Camera ()->getFieldViewAngleRad ();
}


void SphericalCamera::SetFieldOfViewAngleImpl (double i_angle) {
	if (!impl_->camera) {
		return;
	}

	Camera ()->setFieldViewAngleRad (i_angle);
}


QVector3D SphericalCamera::ViewDirectionImpl () const noexcept {
	if (!impl_->camera) {
		return QVector3D{};
	}

	return FromChai3dToOpenGl (Camera ()->getLookVector ());
}


void SphericalCamera::SetViewDirectionImpl (const QVector3D& i_view_direction) {
	if (!impl_->camera) {
		return;
	}

	auto camera = Camera ();

	camera->set (
	    camera->getLocalPos (),
	    FromOpenGlToChai3d (i_view_direction.normalized ()),
	    camera->getUpVector ());
}


QVector3D SphericalCamera::RightDirectionImpl () const noexcept {
	if (!impl_->camera) {
		return QVector3D{};
	}

	return FromChai3dToOpenGl (Camera ()->getRightVector ());
}


void SphericalCamera::SetRightDirectionImpl (
    const QVector3D& i_right_direction) {
	if (!impl_->camera) {
		return;
	}

	auto camera = Camera ();

	auto local_rotation = camera->getLocalRot ();

	local_rotation.setCol1 (
	    FromOpenGlToChai3d (i_right_direction.normalized ()));

	camera->setLocalRot (local_rotation);
}


QVector3D SphericalCamera::UpDirectionImpl () const noexcept {
	if (!impl_->camera) {
		return QVector3D{};
	}

	return FromChai3dToOpenGl (Camera ()->getUpVector ());
}


void SphericalCamera::SetUpDirectionImpl (const QVector3D& i_up_direction) {
	if (!impl_->camera) {
		return;
	}

	auto camera = Camera ();

	camera->set (
	    camera->getLocalPos (),
	    camera->getLookVector (),
	    FromOpenGlToChai3d (i_up_direction.normalized ()));
}


void SphericalCamera::SetReferenceImpl (
    const QVector3D& i_origin,
    const QVector3D& i_azimuth,
    const QVector3D& i_zenith) {
	if (!impl_->camera) {
		return;
	}

	impl_->camera->setSphericalReferences (
	    FromOpenGlToChai3d (i_origin.normalized ()),
	    FromOpenGlToChai3d (i_azimuth.normalized ()),
	    FromOpenGlToChai3d (i_zenith.normalized ()));
}


QVector3D SphericalCamera::OriginImpl () const noexcept {
	if (!impl_->camera) {
		return QVector3D{};
	}

	return FromChai3dToOpenGl (impl_->camera->getSphericalOriginReference ());
}


void SphericalCamera::SetOriginImpl (const QVector3D& i_origin) {
	if (!impl_->camera) {
		return;
	}

	impl_->camera->setSphericalOriginReference (FromOpenGlToChai3d (i_origin));
}


QVector3D SphericalCamera::AzimuthImpl () const noexcept {
	if (!impl_->camera) {
		return QVector3D{};
	}

	return FromChai3dToOpenGl (impl_->camera->getSphericalAzimuthReference ());
}


void SphericalCamera::SetAzimuthImpl (const QVector3D& i_azimuth) {
	if (!impl_->camera) {
		return;
	}

	impl_->camera->setSphericalAzimuthReference (
	    FromOpenGlToChai3d (i_azimuth.normalized ()));
}


QVector3D SphericalCamera::ZenithImpl () const noexcept {
	if (!impl_->camera) {
		return QVector3D{};
	}

	return FromChai3dToOpenGl (impl_->camera->getSphericalZenithReference ());
}


void SphericalCamera::SetZenithImpl (const QVector3D& i_zenith) {
	if (!impl_->camera) {
		return;
	}

	impl_->camera->setSphericalZenithReference (
	    FromOpenGlToChai3d (i_zenith.normalized ()));
}


double SphericalCamera::AzimuthAngleImpl () const noexcept {
	if (!impl_->camera) {
		return 0.0;
	}

	return impl_->camera->getSphericalAzimuthRad ();
}


void SphericalCamera::SetAzimuthAngleImpl (double i_azimuth_angle) {
	if (!impl_->camera) {
		return;
	}

	impl_->camera->setSphericalAzimuthRad (i_azimuth_angle);
}


double SphericalCamera::PolarAngleImpl () const noexcept {
	if (!impl_->camera) {
		return 0.0;
	}

	return impl_->camera->getSphericalPolarRad ();
}


void SphericalCamera::SetPolarAngleImpl (double i_polar_angle) {
	if (!impl_->camera) {
		return;
	}

	impl_->camera->setSphericalPolarRad (i_polar_angle);
}


double SphericalCamera::RadialDistanceImpl () const noexcept {
	if (!impl_->camera) {
		return 0.0;
	}

	return impl_->camera->getSphericalRadius ();
}


void SphericalCamera::SetRadialDistanceImpl (double i_radial_distance) {
	if (!impl_->camera) {
		return;
	}

	impl_->camera->setSphericalRadius (i_radial_distance);
}


BRIDGETTE_QT_CHAI3D_V1_CLOSE_NAMESPACE
