// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Chai3D/BridgetteQtChai3D/include/Bridgette/Qt/Chai3D/v1/OpenGlRenderer.cpp
//
// Description: An OpenGL renderer class integrating Chai3D 3D rendering in Qt.
//              Interface class implementation.


#include "OpenGlRendererImpl.hpp"
#include <utk/idiom/Pimpl_impl.hpp>


#include <mutex>

#include <boost/type_erasure/is_empty.hpp>

#include <utk/exception/LogicError/InvalidLogicArgument.hpp>

#include <world/CWorld.h>


BRIDGETTE_QT_CHAI3D_V1_OPEN_NAMESPACE


OpenGlRenderer::OpenGlRenderer () = default;


OpenGlRenderer::~OpenGlRenderer () = default;


double OpenGlRenderer::FrameRate () const noexcept {
	return impl_->frame_rate_counter->getFrequency ();
}


void OpenGlRenderer::SetCameraAndRenderLock (
    std::weak_ptr< ::chai3d::cCamera > i_camera,
    utk::meta::type_erasure::any_lockable_weak_ptr i_render_lock) {
	if (::boost::type_erasure::is_empty (i_render_lock)) {
		BOOST_THROW_EXCEPTION (
		    utk::exception::InvalidLogicArgument ()
		    << utk::exception::InvalidLogicArgument::ArgumentInfo (
		           utk::exception::InvalidLogicArgument::ArgumentName (
		               "i_render_lock"),
		           utk::exception::InvalidLogicArgument::Description (
		               "Is empty boost::any")));
	}

	impl_->camera = i_camera;
	impl_->render_lock = i_render_lock;
}


::chai3d::cEyeMode OpenGlRenderer::EyeMode () const noexcept {
	return impl_->eye_mode;
}


void OpenGlRenderer::SetEyeMode (::chai3d::cEyeMode i_eye_mode) {
	impl_->eye_mode = i_eye_mode;
}


void OpenGlRenderer::DoInitializeGl () {
#ifdef GLEW_VERSION
	glewInit ();
#endif
}


void OpenGlRenderer::DoPaintGl () {
	auto camera = impl_->camera.lock ();
	auto render_lock_ptr = impl_->render_lock.lock ();

	if (!camera || !render_lock_ptr) {
		return;
	}

	auto render_lock{*render_lock_ptr};

	std::lock_guard< decltype (render_lock) > render_lock_guard{render_lock};

	auto const& viewport_size{impl_->viewport_size};

	auto world = camera->getParentWorld ();

	if (world) {
		world->updateShadowMaps (
		    camera->getMirrorHorizontal (), camera->getMirrorVertical ());
	}

	camera->renderView (
	    viewport_size.width (),
	    viewport_size.height (),
	    impl_->eye_mode,
	    false);

	impl_->frame_rate_counter->signal (1);
}


void OpenGlRenderer::DoResizeGl (int i_width, int i_height) {
	impl_->viewport_size = {i_width, i_height};
}


BRIDGETTE_QT_CHAI3D_V1_CLOSE_NAMESPACE
