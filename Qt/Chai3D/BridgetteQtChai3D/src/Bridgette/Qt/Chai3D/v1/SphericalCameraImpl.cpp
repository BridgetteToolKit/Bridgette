// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Chai3D/BridgetteQtChai3D/include/Bridgette/Qt/Chai3D/v1/SphericalCameraImpl.cpp
//
// Description: The class for controlling the Chai3D camera in spherical
//              coordinates. PIMPL class implementation.


#include "SphericalCameraImpl.hpp"


BRIDGETTE_QT_CHAI3D_V1_OPEN_NAMESPACE


SphericalCamera::Impl::Impl (cCameraSharedPtr i_camera) {
	SetCamera (std::move (i_camera));
}


void SphericalCamera::Impl::SetCamera (cCameraSharedPtr i_camera) {
	camera = std::move (i_camera);

	if (camera) {
		original_camera_fov = camera->getFieldViewAngleRad ();

		zoom_ratio = 1.0;
	}
}


BRIDGETTE_QT_CHAI3D_V1_CLOSE_NAMESPACE
