# Copyright 2018 Bridgette Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# File name: Bridgette/Qt/Chai3D/BridgetteQtChai3D/CMakeLists.txt
#
# Description: Bridgette Qt-Chai3D Integration Library project definition.


project (BridgetteQtChai3D
  LANGUAGES CXX
  VERSION   0.1.0
  )


include (dependencies.cmake)


######################################
# Project target and other variables #
######################################
set (${PROJECT_NAME}_TARGET_NAME                   ${PROJECT_NAME})
set (${PROJECT_NAME}_SECONDARY_TARGET_NAME         ${PROJECT_NAME}_static)
set (${PROJECT_NAME}_TARGET_EXPORT_NAME            "Bridgette::Qt::Chai3D")
set (${PROJECT_NAME}_SECONDARY_TARGET_EXPORT_NAME  "Bridgette::Qt::Chai3D.static")


#####################
# Find dependencies #
#####################
BridgetteQtChai3D_find_dependencies ()


#########################
# Create project target #
#########################
utk_cmake_add_library_targets (
  BUILD_SHARED_LIBRARY        ${${PROJECT_NAME}_BUILD_SHARED_LIBRARY}
  BUILD_STATIC_LIBRARY        ${${PROJECT_NAME}_BUILD_STATIC_LIBRARY}
  DEFAULT_TARGET_TYPE SHARED
  DEFAULT_TARGET_NAME         "${${PROJECT_NAME}_TARGET_NAME}"
  DEFAULT_TARGET_EXPORT_NAME  "${${PROJECT_NAME}_TARGET_EXPORT_NAME}"
  SECONDARY_TARGET_NAME       "${${PROJECT_NAME}_TARGET_NAME}_static"
  SECONDARY_TARGET_NAME       "${${PROJECT_NAME}_SECONDARY_TARGET_EXPORT_NAME}"
  )

# Alias targets are required to use as subproject
add_library (
  ${${PROJECT_NAME}_TARGET_EXPORT_NAME}
  ALIAS
  ${${PROJECT_NAME}_TARGET_NAME}
  )

if (TARGET ${${PROJECT_NAME}_SECONDARY_TARGET_NAME})
  add_library (
    ${${PROJECT_NAME}_SECONDARY_TARGET_EXPORT_NAME}
    ALIAS
    ${${PROJECT_NAME}_SECONDARY_TARGET_NAME}
    )
endif ()


#################################
# Set project target properties #
#################################
# Build options
set_target_properties (
  ${${PROJECT_NAME}_TARGET_LIST}
  PROPERTIES
  CXX_STANDARD           14
  CXX_STANDARD_REQUIRED  TRUE
  DEFINE_SYMBOL          ${PROJECT_NAME}_BUILD
  FOLDER                 "${PARENT_PROJECT}/Qt/Chai3D"
  )

# Versioning properties
set_target_properties (
  ${${PROJECT_NAME}_TARGET_LIST}
  PROPERTIES
  COMPATIBLE_INTERFACE_STRING  "${PROJECT_NAME}_VERSION"
  SOVERSION                    ${${PROJECT_NAME}_VERSION_MAJOR}
  VERSION                      ${${PROJECT_NAME}_VERSION}
  )

# utk.cmake specific options
set_target_properties (
  ${${PROJECT_NAME}_TARGET_LIST}
  PROPERTIES
  UTK_CMAKE_INCLUDE_PREFIX         "Bridgette/Qt/Chai3D"
  UTK_CMAKE_LANGUAGE               "CXX"
  UTK_CMAKE_PROJECT_CXX_NAMESPACE  "bridgette;qt;chai3d;inline;v1"
  )


utk_cmake_name_mangling_postfix (
  TARGET "${${PROJECT_NAME}_TARGET_LIST}"
  TARGET_VERSION_MANGLING
  )


set_target_properties (
  ${${PROJECT_NAME}_TARGET_LIST}
  PROPERTIES
  AUTOMOC ON
  AUTORCC ON
  AUTOUIC ON
  )


####################
# Use dependencies #
####################
BridgetteQtChai3D_use_dependencies (
  TARGET ${${PROJECT_NAME}_TARGET_LIST})



#################
# Export header #
#################
utk_cmake_generate_export_header (
  TARGET             ${${PROJECT_NAME}_TARGET_LIST}
  COMMON_BASE_NAME   "${PROJECT_NAME}"
  )


##########################
# Versioning information #
##########################
utk_cmake_product_information (
  TARGET                    ${${PROJECT_NAME}_TARGET_NAME}
  COMMON_TARGET_IDENTIFIER  "${PROJECT_NAME}"
  CREATE_PRODUCT_INFO_FUNCTIONS
  NAME_STRING               "Bridgette Tool Kit Qt-Chai3D Integration Helper Library"
  CONTRIBUTORS_FILE         "${${PROJECT_NAME}_DIRECTORY}/CONTRIBUTORS"
  COPYRIGHT_HOLDER_STRING   "Bridgette Tool Kit Open Source Contributors"

# Commented because the library is just released and there is no year range for
# copyright info string.

#  COPYRIGHT_INFO_FILE "product_info/COPYRIGHT_INFO"
#  COPYRIGHT_INFO_UTF8_FILE "product_info/COPYRIGHT_INFO"
  LICENSE_FILE              "${PARENT_PROJECT_DIR}/LICENSE"
  LICENSE_NAME_STRING       "Apache License 2.0"
#  NOTICE_FILE               "NOTICE"  # There is no NOTICE file right now
  )


##########################################
# Include directories and link libraries #
##########################################
utk_cmake_target_include_directories(
  TARGET  ${${PROJECT_NAME}_TARGET_LIST}
  PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
  PUBLIC
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>/include
  $<INSTALL_INTERFACE:include>
  PUBLIC
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
  )


#########################################################
# Subdirectories with headers, sources, resources, etc. #
#########################################################
add_subdirectory (include)
add_subdirectory (src)


##########################################
# Install and export project and targets #
##########################################
include (GNUInstallDirs)

utk_cmake_install_project (
  TARGET           ${${PROJECT_NAME}_TARGET_NAME}
  INSTALL_RUNTIME  ${${PROJECT_NAME}_INSTALL_RUNTIME}
  INSTALL_DEVEL    ${${PROJECT_NAME}_INSTALL_DEVEL}
  INSTALL_SOURCES  ${${PROJECT_NAME}_INSTALL_DEVEL}

  ARCHIVE_OPTIONS
  ARCHIVE DESTINATION   "${CMAKE_INSTALL_LIBDIR}"
  INCLUDES DESTINATION  "${CMAKE_INSTALL_INCLUDEDIR}"
  COMPONENT             "Devel"

  LIBRARY_OPTIONS
  ARCHIVE DESTINATION   "${CMAKE_INSTALL_LIBDIR}"
  INCLUDES DESTINATION  "${CMAKE_INSTALL_INCLUDEDIR}"
  COMPONENT             "Devel"

  RUNTIME_OPTIONS
  RUNTIME DESTINATION  "${CMAKE_INSTALL_BINDIR}"
  COMPONENT            "Runtime"

  CUSTOM_CMAKE_CONFIG_FILE  "${CMAKE_CURRENT_LIST_DIR}/cmake/cmake-target-config.cmake.in"

  CMAKE_CONFIG_FILE_OPTIONS
  INSTALL_DESTINATION  "${CMAKE_INSTALL_LIBDIR}/cmake/@TARGET_NAME@"

  CMAKE_CONFIG_VERSION_FILE_OPTIONS
  COMPATIBILITY  "ExactVersion" # For initial development, change to
                                # "SameMajorVersion" after the interface is
                                # stable.

  CMAKE_PACKAGE_FILE_OPTIONS
  COMPONENT    "Devel"
  DESTINATION  "${CMAKE_INSTALL_LIBDIR}/cmake/@TARGET_NAME@"
  )
