# Copyright 2018 Bridgette Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# File name: Bridgette/Qt/Chai3D/BridgetteQtChai3D/dependencies.cmake
#
# Description: Bridgette Qt-Chai3D Integration Library dependencies.


##############################
# Find dependencies function #
##############################
function (BridgetteQtChai3D_find_dependencies)
  set (_options
    USE_FIND_DEPENDENCIES
    )
  set (_multi_value_args
    ""
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})


  if (i_USE_FIND_DEPENDENCIES)
    macro (find_function)
      include (CMakeFindDependencyMacro)

      find_dependency (${ARGN})
    endmacro (find_function)
  else ()
    macro (find_function)
      find_package (${ARGN})
    endmacro (find_function)
  endif ()

  find_function (BridgetteChai3D  0.1.0  REQUIRED)
  find_function (BridgetteQt      0.1.0  REQUIRED)

  utk_cmake_find_or_download_package (
    PACKAGE utk_exception
    FOLDER "Dependencies/utk"
    DOWNLOAD_AND_BUILD_BY_DEFAULT
    FIND_PACKAGE_OPTIONS
    0.3.0 REQUIRED
    DOWNLOAD_OPTIONS
    GIT_TAG v0.3.0
    GIT_REPOSITORY https://gitlab.com/UtilityToolKit/utk.exception.git
    DOWNLOAD_OPTIONS_WITH_OVERRIDE
    GIT_TAG
    GIT_REPOSITORY
    )

  utk_cmake_find_or_download_package (
    PACKAGE utk_idiom
    FOLDER "Dependencies/utk"
    DOWNLOAD_AND_BUILD_BY_DEFAULT
    FIND_PACKAGE_OPTIONS
    0.2.1 REQUIRED
    DOWNLOAD_OPTIONS
    GIT_TAG v0.2.1
    GIT_REPOSITORY https://gitlab.com/UtilityToolKit/utk.idiom.git
    DOWNLOAD_OPTIONS_WITH_OVERRIDE
    GIT_TAG
    GIT_REPOSITORY
    )

  utk_cmake_find_or_download_package (
    PACKAGE utk_meta
    FOLDER "Dependencies/utk"
    DOWNLOAD_AND_BUILD_BY_DEFAULT
    FIND_PACKAGE_OPTIONS
    0.1.0 REQUIRED
    DOWNLOAD_OPTIONS
    GIT_TAG v0.1.0
    GIT_REPOSITORY https://gitlab.com/UtilityToolKit/utk.meta.git
    DOWNLOAD_OPTIONS_WITH_OVERRIDE
    GIT_TAG
    GIT_REPOSITORY
    )
endfunction (BridgetteQtChai3D_find_dependencies)


#############################
# Use dependencies function #
#############################
function (BridgetteQtChai3D_use_dependencies)
  set (_options
    ""
    )
  set (_multi_value_args
    TARGET
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})

  if (NOT i_TARGET)
    message (SEND_ERROR "Provide TARGET argument")
  endif ()

  foreach (_target IN LISTS i_TARGET)
    target_link_libraries (
      ${_target}
      PUBLIC
      Bridgette::Chai3D
      Bridgette::Qt
      utk::exception
      utk::idiom
      utk::meta
      )
  endforeach (_target IN LISTS _interface_targets)
endfunction (BridgetteQtChai3D_use_dependencies)
