// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Chai3D/BridgetteQtChai3D/include/Bridgette/Qt/Chai3D/v1/cCameraPtr.hpp
//
// Description: Declaration of smart pointer types and corresponding factory
//              functions for chai3d::cCamera.


#ifndef QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_CCAMERAPTR_HPP
#define QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_CCAMERAPTR_HPP


#include <memory>

#include "Bridgette/Qt/Chai3D/BridgetteQtChai3D_export.h"
#include "Bridgette/Qt/Chai3D/v1/namespace.hpp"


BRIDGETTE_QT_CHAI3D_V1_OPEN_NAMESPACE


using cCameraSharedPtr = std::shared_ptr< ::chai3d::cCamera >;


template < typename... Args >
static auto MakeSharedCCamera (Args... i_args) -> decltype (
    std::make_shared< ::chai3d::cCamera > (std::forward< Args > (i_args)...)) {
	return std::make_shared< ::chai3d::cCamera > (
	    std::forward< Args > (i_args)...);
};


using cCameraUniquePtr = std::unique_ptr< ::chai3d::cCamera >;


template < typename... Args >
static auto MakeUniqueCCamera (Args... i_args) -> decltype (
    std::make_unique< ::chai3d::cCamera > (std::forward< Args > (i_args)...)) {
	return std::make_unique< ::chai3d::cCamera > (
	    std::forward< Args > (i_args)...);
};


using WeakPtr = std::weak_ptr< ::chai3d::cCamera >;


BRIDGETTE_QT_CHAI3D_V1_CLOSE_NAMESPACE


#endif // QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_CCAMERAPTR_HPP
