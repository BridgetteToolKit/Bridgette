// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Chai3D/BridgetteQtChai3D/include/Bridgette/Qt/Chai3D/v1/OpenGlRenderer.hpp
//
// Description: An OpenGL renderer class integrating Chai3D 3D rendering in Qt.


#ifndef QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_OPENGLRENDERER_HPP
#define QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_OPENGLRENDERER_HPP


#include <utk/idiom/Pimpl.hpp>
#include <utk/idiom/TypeSmartPointer.hpp>

// Boost Type Erasure and utk.meta headers MUST be included before Chai3D
// headers. The reason is that at some point Chai3D includes some Microsoft
// headers that define macro "interface" that breaks compilation.
#include <utk/meta/type_erasure/any_simple_lockable.hpp>

// Chai3D header files MUST be includede before QOpenGLWidget because of the
// requirements for inclusion order for gh.h (included by QOpenGLWidget) and
// glew.h (included by Chai3D headers).
#include <display/CCamera.h>

#include <Bridgette/Qt/OpenGlRendererBase.hpp>

#include "Bridgette/Qt/Chai3D/BridgetteQtChai3D_export.h"
#include "Bridgette/Qt/Chai3D/v1/namespace.hpp"


BRIDGETTE_QT_CHAI3D_V1_OPEN_NAMESPACE


/**
   @brief Chai3D OpenGL renderer for using with bridgette::qt::OpenGlWidget and
   bridgette::qt::OpenGlWindow
*/
class BRIDGETTEQTCHAI3D_EXPORT OpenGlRenderer
    : public qt::OpenGlRendererBase {
public:
	UTK_IDIOM_SHARED_PTR (OpenGlRenderer);
	UTK_IDIOM_UNIQUE_PTR (OpenGlRenderer);
	UTK_IDIOM_WEAK_PTR (OpenGlRenderer);

	UTK_IDIOM_MAKE_SHARED (OpenGlRenderer);
	UTK_IDIOM_MAKE_UNIQUE (OpenGlRenderer);


	OpenGlRenderer ();
	~OpenGlRenderer ();

	/**
	   @brief Returns the OpenGL rendering frame rate

	   @returns The OpenGL rendering fram rate.
	*/
	double FrameRate () const noexcept;

	/**
	   @brief Sets the camera used for rendering and the lock object to prevent
	   the scene from changing during the frame rendering

	   @details The i_camera parent world is used for updating shadow map.

	   @param [in] i_camera - the camera for rendering the scene.

	   @param [in] i_render_lock - the synchronisation object for locking the
	   scene to prevent it from changes during the frame rendering.
	*/
	void SetCameraAndRenderLock (
	    std::weak_ptr< ::chai3d::cCamera > i_camera,
	    utk::meta::type_erasure::any_lockable_weak_ptr i_render_lock);

	/**
	   @brief Returns the eye mode used to render the scene

	   @returns The eye mode used to render the scene.
	*/
	::chai3d::cEyeMode EyeMode () const noexcept;

	/**
	   @brief Sets the eye mode for rendering the scene

	   @details The value is used as argument for the
	   chai3d::cCamera::renderView() function call. The main use case for the
	   eye mode is the chai3d::C_STEREO_PASSIVE_DUAL_DISPLAY stereo rendering
	   mode.

	   By default the chai3d::C_STEREO_LEFT_EYE value is used.

	   @param [in] i_eye_mode The eye mode to be used for rendering the scene.
	*/
	void SetEyeMode (::chai3d::cEyeMode i_eye_mode);


private:
	class Impl;
	utk::idiom::Pimpl< Impl > impl_;


	void DoInitializeGl () override;
	void DoPaintGl () override;
	void DoResizeGl (int i_width, int i_height) override;
};


BRIDGETTE_QT_CHAI3D_V1_CLOSE_NAMESPACE


#endif // QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_OPENGLRENDERER_HPP
