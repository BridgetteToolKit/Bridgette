// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Chai3D/BridgetteQtChai3D/include/Bridgette/Qt/Chai3D/v1/Chai3dOpenGlVectorConversion.hpp
//
// Description: Convenience functions for transforming vectors from Chai3D
//              coordinate system to OpenGL coordinate system and vice versa.


#ifndef QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_CHAI3DOPENGLVECTORCONVERSION_HPP
#define QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_CHAI3DOPENGLVECTORCONVERSION_HPP


#include <QVector3D>

#include <math/CVector3d.h>

#include "Bridgette/Qt/Chai3D/v1/namespace.hpp"


BRIDGETTE_QT_CHAI3D_V1_OPEN_NAMESPACE


inline QVector3D
    FromChai3dToOpenGl (const ::chai3d::cVector3d& i_chai3d_vector) noexcept {
	return {i_chai3d_vector.y (), i_chai3d_vector.z (), i_chai3d_vector.x ()};
}


inline ::chai3d::cVector3d
    FromOpenGlToChai3d (const QVector3D& i_opengl_vector) noexcept {
	return {i_opengl_vector.z (), i_opengl_vector.x (), i_opengl_vector.y ()};
}


BRIDGETTE_QT_CHAI3D_V1_CLOSE_NAMESPACE


#endif // QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_CHAI3DOPENGLVECTORCONVERSION_HPP
