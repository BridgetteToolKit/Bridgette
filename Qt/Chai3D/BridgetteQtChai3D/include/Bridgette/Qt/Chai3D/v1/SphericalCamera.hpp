// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Chai3D/BridgetteQtChai3D/include/Bridgette/Qt/Chai3D/v1/SphericalCamera.hpp
//
// Description: The class for controlling the Chai3D camera in spherical
//              coordinates.


#ifndef QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_SPHERICALCAMERA_HPP
#define QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_SPHERICALCAMERA_HPP


#include <utk/idiom/Pimpl.hpp>
#include <utk/idiom/TypeSmartPointer.hpp>

#include <display/CCamera.h>

#include "Bridgette/Qt/ISphericalCamera.hpp"

#include "Bridgette/Qt/Chai3D/BridgetteQtChai3D_export.h"
#include "Bridgette/Qt/Chai3D/v1/namespace.hpp"

#include "Bridgette/Qt/Chai3D/v1/cCameraPtr.hpp"


BRIDGETTE_QT_CHAI3D_V1_OPEN_NAMESPACE


/**
   @brief The class for controlling the Chai3D camera in spherical coordinates
*/
class BRIDGETTEQTCHAI3D_EXPORT SphericalCamera
    : public bridgette::qt::ISphericalCamera {
public:
	UTK_IDIOM_SHARED_PTR (SphericalCamera);
	UTK_IDIOM_UNIQUE_PTR (SphericalCamera);
	UTK_IDIOM_WEAK_PTR (SphericalCamera);

	UTK_IDIOM_MAKE_SHARED (SphericalCamera);
	UTK_IDIOM_MAKE_UNIQUE (SphericalCamera);


	explicit SphericalCamera (cCameraSharedPtr i_camera = cCameraSharedPtr{});
	~SphericalCamera ();

	::chai3d::cCamera* Camera () const noexcept;
	void SetCamera (cCameraSharedPtr i_camera);


private:
	class Impl;
	utk::idiom::Pimpl< Impl > impl_;

	double FieldOfViewAngleImpl () const noexcept override;
	void SetFieldOfViewAngleImpl (double i_angle) override;

	QVector3D ViewDirectionImpl () const noexcept override;
	void SetViewDirectionImpl (const QVector3D& i_view_direction) override;

	QVector3D RightDirectionImpl () const noexcept override;
	void SetRightDirectionImpl (const QVector3D& i_right_direction) override;

	QVector3D UpDirectionImpl () const noexcept override;
	void SetUpDirectionImpl (const QVector3D& i_up_direction) override;

	void SetReferenceImpl (
	    const QVector3D& i_origin,
	    const QVector3D& i_azimuth,
	    const QVector3D& i_zenith) override;

	QVector3D OriginImpl () const noexcept override;
	void SetOriginImpl (const QVector3D& i_origin) override;

	QVector3D AzimuthImpl () const noexcept override;
	void SetAzimuthImpl (const QVector3D& i_azimuth) override;

	QVector3D ZenithImpl () const noexcept override;
	void SetZenithImpl (const QVector3D& i_zenith) override;

	double AzimuthAngleImpl () const noexcept override;
	void SetAzimuthAngleImpl (double i_azimuth_angle) override;

	double PolarAngleImpl () const noexcept override;
	void SetPolarAngleImpl (double i_polar_angle) override;

	double RadialDistanceImpl () const noexcept override;
	void SetRadialDistanceImpl (double i_radial_distance) override;
};


BRIDGETTE_QT_CHAI3D_V1_CLOSE_NAMESPACE


#endif // QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_V1_SPHERICALCAMERA_HPP
