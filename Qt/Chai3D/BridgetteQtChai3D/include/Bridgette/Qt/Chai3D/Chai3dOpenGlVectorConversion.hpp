// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Chai3D/BridgetteQtChai3D/include/Bridgette/Qt/Chai3D/v1/Chai3dOpenGlVectorConversion.hpp
//
// Description: Convenience functions for transforming vectors from Chai3D
//              coordinate system to OpenGL coordinate system and vice
//              versa. Interface header file


#ifndef QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_CHAI3DOPENGLVECTORCONVERSION_HPP
#define QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_CHAI3DOPENGLVECTORCONVERSION_HPP


#include "Bridgette/Qt/Chai3D/v1/Chai3dOpenGlVectorConversion.hpp"


#endif // QT_CHAI3D_BRIDGETTEQTCHAI3D_INCLUDE_BRIDGETTE_QT_CHAI3D_CHAI3DOPENGLVECTORCONVERSION_HPP
