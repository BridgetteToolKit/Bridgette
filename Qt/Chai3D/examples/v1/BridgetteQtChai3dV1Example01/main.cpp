#include <mutex>

#include <QApplication>
#include <QSurfaceFormat>

#include <utk/idiom/SynchronisedObject.hpp>


#include <Bridgette/Qt/Chai3D/OpenGlRenderer.hpp>
#include <Bridgette/Qt/Chai3D/SphericalCamera.hpp>

#include <Bridgette/Qt/OpenGlWidget.hpp>

#include <Bridgette/Qt/CameraControllerOperatorConnection.hpp>
#include <Bridgette/Qt/MouseCameraController.hpp>
#include <Bridgette/Qt/SphericalCameraOperator.hpp>

#include <Bridgette/Chai3D/ObjectFollower.hpp>


#include <devices/CGenericHapticDevice.h>
#include <devices/CHapticDeviceHandler.h>
#include <display/CCamera.h>
#include <lighting/CSpotLight.h>
#include <tools/CToolCursor.h>
#include <world/CShapeLine.h>
#include <world/CShapeSphere.h>
#include <world/CWorld.h>

#include "CreatePrimitives.hpp"


using SynchronisedWorld =
    utk::idiom::SynchronisedObject< ::chai3d::cWorld, std::mutex >;

using ToolCameraFollower = bridgette::chai3d::
    ObjectFollower< ::chai3d::cToolCursor, ::chai3d::cCamera >;


int main (int i_argc, char** i_argv) {
	const ::chai3d::cStereoMode stereo_mode = ::chai3d::C_STEREO_DISABLED;
	const bool mirrored_display = false;

	// Create a new world.
	auto world = SynchronisedWorld::MakeShared ();

	// Set the background color of the environment
	world->m_backgroundColor.setWhite ();

	// Create a camera and insert it into the virtual world
	auto camera = std::shared_ptr< ::chai3d::cCamera >{
	    new ::chai3d::cCamera{world.get ()}};

	/*
	  No need to add to the world because the camera is managed by
	  std::shared_ptr
	*/
	// world->addChild (camera.get ());

	{ // Setup camera
		// Position and orient the camera
		camera->set (
		    ::chai3d::cVector3d (0.9, 0.0, 0.6), // camera position (eye)
		    ::chai3d::cVector3d (0.0, 0.0, 0.0), // look at position (target)
		    ::chai3d::cVector3d (
		        0.0, 0.0, 1.0)); // direction of the (up) vector

		// Set the near and far clipping planes of the camera
		camera->setClippingPlanes (0.01, 10.0);

		// Set stereo mode
		camera->setStereoMode (stereo_mode);

		// Set stereo eye separation and focal length (applies only if stereo is
		// enabled)
		camera->setStereoEyeSeparation (0.01);
		camera->setStereoFocalLength (0.5);

		// Set vertical mirrored display mode
		camera->setMirrorVertical (mirrored_display);
	}

	{ // Setup light
	  // Create a spot light source
		auto light = new ::chai3d::cSpotLight (world.get ());

		world->addChild (light);

		// Enable light source
		light->setEnabled (true);

		// Position of the light source
		light->setLocalPos (0.6, 0.6, 0.5);

		// Define direction of light beam
		light->setDir (-0.5, -0.5, -0.5);

		// Enable shadows
		light->setShadowMapEnabled (true);

		// The shadow map resolution. May also be low and medium.
		light->m_shadowMap->setQualityHigh ();

		// The light cone half angle
		light->setCutOffAngleDeg (30);
	}

	auto initialize_gl =
	    CreatePrimitives (world.get (), nullptr, 1.0, 0.005, 0);

	// Create viewport widget and run the application
	QApplication app (i_argc, i_argv);

	auto main_window = std::make_unique< ::bridgette::qt::OpenGlWidget > ();

	/*
	  Spherical camera wrapper object for the Chai3D camera to enable
	  controlling it with a mouse.
	*/
	auto spherical_camera =
	    ::bridgette::qt::chai3d::SphericalCamera::MakeShared (camera);


	//  Spherical camera operator to operate the spherical camera.
	auto spherical_camera_operator =
	    ::bridgette::qt::SphericalCameraOperator::MakeShared (spherical_camera);

	// User input handler for controlling the camera.
	auto mouse_camera_controller =
	    ::bridgette::qt::MouseCameraController::MakeShared ();

	// Scale is required to get reasonable position and angle change speed.
	mouse_camera_controller->setDollyScale (0.01);
	mouse_camera_controller->setMovementScale (0.01);
	mouse_camera_controller->setTrackingScale (0.01);
	mouse_camera_controller->setTurningScale (0.1);
	mouse_camera_controller->setZoomScale (0.1);

	// Control the camera with a mouse.
	::bridgette::qt::ConnectControllerToCameraOperator (
	    mouse_camera_controller.get (), spherical_camera_operator.get ());

	{ // Setup renderer
		auto chai3d_renderer =
		    ::bridgette::qt::chai3d::OpenGlRenderer::MakeUnique ();

		chai3d_renderer->SetCameraAndRenderLock (
		    camera, SynchronisedWorld::WeakPtr{world});

		chai3d_renderer->SetInitializeGlFunction (initialize_gl);

		main_window->SetRenderer (std::move (chai3d_renderer));

		main_window->installEventFilter (mouse_camera_controller.get ());
	}

	main_window->show ();

	return app.exec ();
}
