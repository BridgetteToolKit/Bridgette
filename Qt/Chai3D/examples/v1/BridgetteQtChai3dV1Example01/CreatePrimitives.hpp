//==============================================================================
/*
    Software License Agreement (BSD License)
    Copyright (c) 2003-2016, CHAI3D.
    (www.chai3d.org)

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.

    * Neither the name of CHAI3D nor the names of its contributors may
    be used to endorse or promote products derived from this software
    without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

    \author    <http://www.chai3d.org>
    \author    Francois Conti
    \version   3.2.0 $Rev: 1925 $
*/


#ifndef QT_CHAI3D_EXAMPLES_V1_BRIDGETTEQTCHAI3DV1OPENGLRENDERER_CREATEPRIMITIVES_HPP
#define QT_CHAI3D_EXAMPLES_V1_BRIDGETTEQTCHAI3DV1OPENGLRENDERER_CREATEPRIMITIVES_HPP


#include <functional>

#include <graphics/CPrimitives.h>

#include <resources/CShaderFong.h>

#include <shaders/CShader.h>
#include <shaders/CShaderProgram.h>

#include <tools/CGenericTool.h>

#include <world/CMesh.h>
#include <world/CMultiSegment.h>
#include <world/CWorld.h>


inline std::function< void() > CreatePrimitives (
    ::chai3d::cWorld* o_world,
    ::chai3d::cGenericTool* o_tool,
    double i_scale_factor,
    double i_max_linear_stiffness,
    double i_tool_radius) {
	//--------------------------------------------------------------------------
	// CREATE OBJECTS
	//--------------------------------------------------------------------------
	double workspaceScaleFactor = i_scale_factor;

	// stiffness properties
	double maxStiffness = i_max_linear_stiffness / workspaceScaleFactor;


	/////////////////////////////////////////////////////////////////////////
	// BASE
	/////////////////////////////////////////////////////////////////////////

	// create a mesh
	::chai3d::cMesh* base = new ::chai3d::cMesh ();

	// add object to world
	o_world->addChild (base);

	// build mesh using a cylinder primitive
	::chai3d::cCreateCylinder (
	    base,
	    0.01,
	    0.5,
	    36,
	    1,
	    10,
	    true,
	    true,
	    ::chai3d::cVector3d (0.0, 0.0, -0.01),
	    ::chai3d::cMatrix3d (
	        ::chai3d::cDegToRad (0),
	        ::chai3d::cDegToRad (0),
	        ::chai3d::cDegToRad (0),
	        ::chai3d::C_EULER_ORDER_XYZ));

	// set material properties
	base->m_material->setGrayGainsboro ();
	base->m_material->setStiffness (0.5 * maxStiffness);

	if (o_tool) {
		// build collision detection tree
		base->createAABBCollisionDetector (i_tool_radius);
	}

	// use display list to optimize graphic rendering performance
	base->setUseDisplayList (true);


	/////////////////////////////////////////////////////////////////////////
	// TEA POT
	/////////////////////////////////////////////////////////////////////////

	// create a mesh
	::chai3d::cMesh* teaPot = new ::chai3d::cMesh ();

	// add object to world
	base->addChild (teaPot);

	// build mesh using a cylinder primitive
	::chai3d::cCreateTeaPot (
	    teaPot,
	    0.5,
	    4,
	    ::chai3d::cVector3d (0.0, 0.0, 0.0),
	    ::chai3d::cMatrix3d (
	        ::chai3d::cDegToRad (0),
	        ::chai3d::cDegToRad (0),
	        ::chai3d::cDegToRad (-90),
	        ::chai3d::C_EULER_ORDER_XYZ));

	// position object
	teaPot->setLocalPos (0.1, 0.2, 0.0);

	// set material properties
	teaPot->m_material->setRedDark ();
	teaPot->m_material->setStiffness (0.5 * maxStiffness);

	if (o_tool) {
		// build collision detection tree
		teaPot->createAABBCollisionDetector (i_tool_radius);
	}

	// use display list to optimize graphic rendering performance
	teaPot->setUseDisplayList (true);


	/////////////////////////////////////////////////////////////////////////
	// CYLINDER
	/////////////////////////////////////////////////////////////////////////

	// create a mesh
	::chai3d::cMesh* cylinder = new ::chai3d::cMesh ();

	// add object to world
	base->addChild (cylinder);

	// build mesh using a cylinder primitive
	::chai3d::cCreatePipe (
	    cylinder,
	    0.15,
	    0.05,
	    0.06,
	    32,
	    1,
	    ::chai3d::cVector3d (-0.05, -0.20, 0.0),
	    ::chai3d::cMatrix3d (
	        ::chai3d::cDegToRad (0),
	        ::chai3d::cDegToRad (0),
	        ::chai3d::cDegToRad (170),
	        ::chai3d::C_EULER_ORDER_XYZ));

	// set material properties
	cylinder->m_material->setBlueCornflower ();
	cylinder->m_material->setStiffness (0.5 * maxStiffness);

	if (o_tool) {
		// build collision detection tree
		cylinder->createAABBCollisionDetector (i_tool_radius);
	}

	// use display list to optimize graphic rendering performance
	cylinder->setUseDisplayList (true);


	/////////////////////////////////////////////////////////////////////////
	// CONE
	/////////////////////////////////////////////////////////////////////////

	// create a mesh
	::chai3d::cMesh* cone = new ::chai3d::cMesh ();

	// add object to world
	base->addChild (cone);

	// build mesh using a cylinder primitive
	::chai3d::cCreateCone (
	    cone,
	    0.15,
	    0.05,
	    0.01,
	    32,
	    1,
	    1,
	    true,
	    true,
	    ::chai3d::cVector3d (0.30, 0.0, 0.0),
	    ::chai3d::cMatrix3d (
	        ::chai3d::cDegToRad (0),
	        ::chai3d::cDegToRad (0),
	        ::chai3d::cDegToRad (0),
	        ::chai3d::C_EULER_ORDER_XYZ));

	// set material properties
	cone->m_material->setGreenForest ();
	cone->m_material->setStiffness (0.5 * maxStiffness);

	if (o_tool) {
		// build collision detection tree
		cone->createAABBCollisionDetector (i_tool_radius);
	}

	// use display list to optimize graphic rendering performance
	cone->setUseDisplayList (true);


	/////////////////////////////////////////////////////////////////////////
	// SEGMENTS
	/////////////////////////////////////////////////////////////////////////

	// create a line segment object
	::chai3d::cMultiSegment* segments = new ::chai3d::cMultiSegment ();

	// add object to world
	base->addChild (segments);

	// build some segment
	double l = 0.0;
	double dl = 0.001;
	double a = 0.0;
	double da = 0.2;
	double r = 0.05;

	for (int i = 0; i < 200; i++) {
		double px0 = r * cos (a);
		double py0 = r * sin (a);
		double pz0 = l;

		double px1 = r * cos (a + da);
		double py1 = r * sin (a + da);
		double pz1 = l + dl;

		// create vertex 0
		int index0 = segments->newVertex (px0, py0, pz0);

		// create vertex 1
		int index1 = segments->newVertex (px1, py1, pz1);

		// create segment
		segments->newSegment (index0, index1);

		l = l + dl;
		a = a + da;
	}

	// set haptic properties
	segments->m_material->setStiffness (0.5 * maxStiffness);

	// position object
	segments->setLocalPos (0.22, -0.22, 0.0);

	// set segment properties
	::chai3d::cColorf color;
	color.setYellowGold ();
	segments->setLineColor (color);
	segments->setLineWidth (4.0);
	segments->setUseDisplayList (true);

	if (o_tool) {
		// build collision detection tree
		segments->createAABBCollisionDetector (i_tool_radius);
	}

	// use display list to optimize graphic rendering performance
	segments->setUseDisplayList (true);

	return [base, teaPot, cylinder, cone, o_tool]() {
		//--------------------------------------------------------------------------
		// CREATE SHADERS
		//--------------------------------------------------------------------------

		auto shaderProgram = ::chai3d::cShaderProgram::create (
		    ::chai3d::C_SHADER_FONG_VERT, ::chai3d::C_SHADER_FONG_FRAG);

		if (shaderProgram->getId () > 0) {
			// set uniforms
			shaderProgram->setUniformi ("uShadowMap", C_TU_SHADOWMAP);

			// assign shader to mesh objects in the world
			if (o_tool) {
				o_tool->setShaderProgram (shaderProgram);
			}

			base->setShaderProgram (shaderProgram);
			teaPot->setShaderProgram (shaderProgram);
			cylinder->setShaderProgram (shaderProgram);
			cone->setShaderProgram (shaderProgram);
		}
	};
}


#endif // QT_CHAI3D_EXAMPLES_V1_BRIDGETTEQTCHAI3DV1OPENGLRENDERER_CREATEPRIMITIVES_HPP
