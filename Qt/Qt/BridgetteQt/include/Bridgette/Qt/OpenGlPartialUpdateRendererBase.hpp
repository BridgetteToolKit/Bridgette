// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/OpenGlPartialUpdateRendererBase.hpp
//
// Description: A convenience base class for classes implementing the
//              IOpenGlPartialUpdateRenderer interface. Interface header file.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_OPENGLPARTIALUPDATERENDERERBASE_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_OPENGLPARTIALUPDATERENDERERBASE_HPP


#include "Bridgette/Qt/v1/OpenGlPartialUpdateRendererBase.hpp"


#endif /* QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_OPENGLPARTIALUPDATERENDERERBASE_HPP */
