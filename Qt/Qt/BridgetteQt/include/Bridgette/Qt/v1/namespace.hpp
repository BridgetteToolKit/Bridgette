// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name: Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/namespace.txt
//
// Description: Bridgette Qt Integration Library namespace declaration.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_NAMESPACE_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_NAMESPACE_HPP


#define BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE                                      \
	namespace bridgette {                                                      \
		namespace qt {                                                         \
			inline namespace v1 {

#define BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE                                     \
	}                                                                          \
	}                                                                          \
	}


#endif /* QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_NAMESPACE_HPP */
