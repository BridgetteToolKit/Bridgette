// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/OpenGlWindow.hpp
//
// Description: The window class for OpenGL rendering performed by the software
//              that is being integrated with Qt.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_OPENGLWINDOW_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_OPENGLWINDOW_HPP


#include <QOpenGLWindow>

#include <utk/idiom/Pimpl.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"

#include "Bridgette/Qt/v1/GenericOpenGlUiComponentBase.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief The window class for OpenGL rendering performed by the software that
   is being integrated with Qt

   @details The class is intended to be used in cases where only the basic
   IOpenGlView API is required. In some cases, it may be desirable to have an
   IOpenGlView-based class with some specific API that is required by the task
   at hand. For these cases, it is recommended to create a new window class
   based on OpenGlWindowBase that enforces the use of only specific OpenGL
   renderer classes in a type-safe manner.
*/
class BRIDGETTEQT_EXPORT OpenGlWindow
    : GenericOpenGlUiComponentBase< ::QOpenGLWindow > {
public:
	explicit OpenGlWindow (
	    QOpenGLWindow::UpdateBehavior i_update_behavior = NoPartialUpdate,
	    QWindow* i_parent = nullptr);

	explicit OpenGlWindow (
	    QOpenGLContext* i_share_context,
	    QOpenGLWindow::UpdateBehavior i_update_behavior = NoPartialUpdate,
	    QWindow* i_parent = nullptr);

	~OpenGlWindow ();


	/**
	   @brief Returns the rendererport object used to perform OpenGL partial
	   update rendering in this window

	   @returns The rendererport object used to perform OpenGL partial update
	   rendering in this window or nullptr if no rendererport object was set.
	 */
	IOpenGlPartialUpdateRenderer* Renderer () const;

	/**
	   @brief Sets the rendererport object used to perform OpenGL partial
	   rendering in this window

	   @param [in] i_opengl_renderer The rendererport object for using to
	   perform OpenGL partial update rendering in this window.
	 */
	void
	    SetRenderer (IOpenGlPartialUpdateRenderer::UniquePtr i_opengl_renderer);


private:
	using Base = GenericOpenGlUiComponentBase< ::QOpenGLWindow >;

	class Impl;
	utk::idiom::Pimpl< Impl > impl_;
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif /* QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_OPENGLWINDOW_HPP */
