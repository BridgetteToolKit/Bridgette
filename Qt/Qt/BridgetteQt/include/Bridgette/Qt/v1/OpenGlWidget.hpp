// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/OpenGlWidget.hpp
//
// Description: The widget class for OpenGL rendering performed by the software
//              that is being integrated with Qt


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_OPENGLWIDGET_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_OPENGLWIDGET_HPP


#include <QOpenGLWidget>

#include <utk/idiom/Pimpl.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"

#include "Bridgette/Qt/v1/GenericOpenGlUiComponentBase.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief The widget class for OpenGL rendering performed by the software that
   is being integrated with Qt

   @details The class is intended to be used in cases where only the basic
   IOpenGlRenderer API is required. In some cases, it may be desirable to have
   an IOpenGlRenderer-based class with some specific API that is required by the
   task at hand. For these cases, it is recommended to create a new widget class
   based on OpenGlWidgetBase that enforces the use of only specific OpenGL
   renderer classes in a type-safe manner.
*/
class BRIDGETTEQT_EXPORT OpenGlWidget
    : public GenericOpenGlUiComponentBase< ::QOpenGLWidget > {
public:
	explicit OpenGlWidget (QWidget* i_parent = nullptr);
	~OpenGlWidget ();


	/**
	   @brief Returns the rendererport object used to perform OpenGL rendering
	   in this widget

	   @returns The rendererport object used to perform OpenGL rendering in
	   this widget or nullptr if no rendererport object was set.
	 */
	IOpenGlRenderer* Renderer () const;

	/**
	   @brief Sets the rendererport object used to perform OpenGL rendering in
	   this widget

	   @param [in] i_opengl_renderer The rendererport object for using to
	   perform OpenGL rendering in this widget.
	 */
	void SetRenderer (IOpenGlRenderer::UniquePtr i_opengl_renderer);


private:
	using Base = GenericOpenGlUiComponentBase< ::QOpenGLWidget >;

	class Impl;
	utk::idiom::Pimpl< Impl > impl_;
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif /* QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_OPENGLWIDGET_HPP */
