// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/CameraOperator.hpp
//
// Description: The base class for camera operators, controlling virtual cameras
//              or viewpoints.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_CAMERAOPERATOR_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_CAMERAOPERATOR_HPP


#include <QObject>

#include <QVector2D>
#include <QVector3D>

#include <utk/idiom/Pimpl.hpp>
#include <utk/idiom/TypeSmartPointer.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"

#include "Bridgette/Qt/v1/ICamera.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief The base class for camera operators, controlling virtual cameras or
   viewpoints

   @details The camera operator processes the commands from the camera
   controller and changes the virtual camera or viewpoint position, orientation
   and parameters accordingly.
*/
class BRIDGETTEQT_EXPORT CameraOperator : public QObject {
	Q_OBJECT

public:
	UTK_IDIOM_SHARED_PTR (CameraOperator);
	UTK_IDIOM_UNIQUE_PTR (CameraOperator);
	UTK_IDIOM_WEAK_PTR (CameraOperator);


	~CameraOperator ();


public Q_SLOTS:
	/**
	   @brief Changes the distance between the camera and the object

	   @param [in] i_dolly The distance change
	*/
	void Dolly (double i_dolly);

	/**
	   @brief Moves the camera by the given vector

	   @param [in] i_move The vector of the camera movement.
	*/
	void Move (QVector3D i_move);

	/**
	   @brief Rotates the camera around its axis

	   @param [in] i_angle The angle of the rotation in radians.
	*/
	void Rotate (double i_angle);

	/**
	   @brief Moves the camera sideways, parallel to an object

	   @param [in] i_track The vector of the camera position change in the plane
	   parallel to the viewport plane.
	*/
	void Track (QVector2D i_track);

	/**
	   @brief Turns by the given direction change

	   @param [in] i_turn The vector of the camera view direction change. The
	   components of the vector represent the change of the direction mapped to
	   the screen plane.
	*/
	void Turn (QVector2D i_turn);

	/**
	   @brief Sets the camera zoom ratio

	   @param [in] i_zoom_ratio The new value of the zoom ratio.
	*/
	void Zoom (double i_zoom_ratio);

	/**
	   @brief Changes the camera zoom ratio

	   @param [in] i_zoom_ratio_change The relative change of the zoom ratio.
	*/
	void ChangeZoom (double i_zoom_ratio_change);


protected:
	explicit CameraOperator (
	    ICamera::SharedPtr i_camera = ICamera::SharedPtr{});

	/**
	   @brief The pointer to the camera under operation

	   @returns The pointer to the camera under operation.
	 */
	ICamera* Camera () const noexcept;

	/**
	   @brief Sets the pointer to the camera under operation

	   @param [in] i_camera The new pointer to the camera to operate on.

	   @param [in] i_keep_zoom If true then the zoom ratio is preserved on
	   camera pointer change.
	 */
	void SetCamera (ICamera::SharedPtr i_camera, bool i_keep_zoom = false);


private:
	class Impl;
	utk::idiom::Pimpl< Impl > impl_;


	virtual void DollyImpl (double i_dolly) = 0;
	virtual void MoveImpl (QVector3D i_move) = 0;
	virtual void RotateImpl (double i_angle) = 0;
	virtual void TrackImpl (QVector2D i_track) = 0;
	virtual void TurnImpl (QVector2D i_turn) = 0;

	/*
	  It is possible to implement setting and changing zoom ratio universally
	  because this operation only requires changing camera's FOV angle access to
	  which is available through the ICamera interface.
	*/
	virtual void ZoomImpl (double i_zoom_ratio);
	virtual void ChangeZoomImpl (double i_zoom_ratio_change);
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_CAMERAOPERATOR_HPP
