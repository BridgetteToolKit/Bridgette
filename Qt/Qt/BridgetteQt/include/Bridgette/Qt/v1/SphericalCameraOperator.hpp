// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/SphericalCameraOperator.hpp
//
// Description: The camera operator that operates in spherical coordinates.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_SPHERICALCAMERAOPERATOR_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_SPHERICALCAMERAOPERATOR_HPP


#include <utk/idiom/Pimpl.hpp>
#include <utk/idiom/TypeSmartPointer.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"

#include "Bridgette/Qt/v1/CameraOperator.hpp"
#include "Bridgette/Qt/v1/ISphericalCamera.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief The camera operator that operates in spherical coordinates
*/
class BRIDGETTEQT_EXPORT SphericalCameraOperator : public CameraOperator {
	Q_OBJECT

public:
	UTK_IDIOM_SHARED_PTR (SphericalCameraOperator);
	UTK_IDIOM_UNIQUE_PTR (SphericalCameraOperator);
	UTK_IDIOM_WEAK_PTR (SphericalCameraOperator);

	UTK_IDIOM_MAKE_SHARED (SphericalCameraOperator);
	UTK_IDIOM_MAKE_UNIQUE (SphericalCameraOperator);


	explicit SphericalCameraOperator (
	    ISphericalCamera::SharedPtr i_camera = ISphericalCamera::SharedPtr{});
	~SphericalCameraOperator ();

	/**
	   @brief The pointer to the camera under operation

	   @returns The pointer to the camera under operation.
	 */
	ISphericalCamera* Camera () const noexcept;

	/**
	   @brief Sets the pointer to the camera under operation

	   @param [in] i_camera The new pointer to the camera to operate on.

	   @param [in] i_keep_zoom If true then the zoom ratio is preserved on
	   camera pointer change.
	*/
	void SetCamera (
	    ISphericalCamera::SharedPtr i_camera, bool i_keep_zoom = false);


private:
	class Impl;
	utk::idiom::Pimpl< Impl > impl_;


	void DollyImpl (double i_dolly);
	void MoveImpl (QVector3D i_move);
	void RotateImpl (double i_angle);
	void TrackImpl (QVector2D i_track);
	void TurnImpl (QVector2D i_turn);
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_SPHERICALCAMERAOPERATOR_HPP
