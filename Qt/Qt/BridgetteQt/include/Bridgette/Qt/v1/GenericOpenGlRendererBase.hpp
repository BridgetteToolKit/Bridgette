// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/GenericOpenGlRendererBase.hpp
//
// Description: A generic base class for classes performing OpenGL rendering.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_GENERICOPENGLRENDERERBASE_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_GENERICOPENGLRENDERERBASE_HPP


#include <functional>
#include <type_traits>

#include <utk/idiom/Pimpl.hpp>

#include "Bridgette/Qt/v1/namespace.hpp"

#include "Bridgette/Qt/v1/IOpenGlPartialUpdateRenderer.hpp"
#include "Bridgette/Qt/v1/IOpenGlRenderer.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief A generic base class for classes performing OpenGL rendering

   @details To create a new class performing OpenGL rendering or integrating
   third-party OpenGL rendering it is required to inherit this class and specify
   the desired interface (IOpenGlRenderer or IOpenGlPartialUpdateRenderer) as
   template argument. In case of IOpenGlPartialUpdateRenderer it is also
   required to provide implementation for the PaintOverGl() and PaintUnderGl()
   functions if they are required. If neither PaintOverGl() nor PaintUnderGl()
   is required then it is possible to implement the IOpenGlRenderer interface
   and use the OpenGlRendererPartialUpdateAdapter to use it where the
   IOpenGlPartialUpdateRenderer is required.
*/
template < class RendererInterface >
class GenericOpenGlRendererBase : public RendererInterface {
	static_assert (
	    std::is_same< IOpenGlRenderer, RendererInterface >::value ||
	        std::is_same< IOpenGlPartialUpdateRenderer, RendererInterface >::
	            value,
	    "The renderer interface should be either IOpenGlRenderer or "
	    "IOpenGlPartialUpdateRenderer.");

public:
	using Interface = RendererInterface;
	using RendererType = GenericOpenGlRendererBase< Interface >;


	/**
	   @brief The type of the function that is used to perform OpenGL resources
	   initialisation
	*/
	using InitializeGlFunction = std::function< void() >;


	explicit GenericOpenGlRendererBase ();

	~GenericOpenGlRendererBase ();

	/**
	   @brief Resets the user provided function for initialising OpenGL
	   resources
	 */
	void ResetInitializeGlFunction ();

	/**
	   @brief Sets the function for initialising OpenGL resources

	   @details The function is called after DoInitializeGl() function in
	   initializeGL() function of the OpenGlWidget or OpenGlWindow classes. This
	   function is intended for the application specific resources
	   initialisation.

	   If some resources initialisation is required for the OpenGL rendering
	   that is being integrated with Qt, then it should be performed in
	   DoInitializeGl() function.

	   If a new function is set then it will be called before the next
	   DoPaintGl() function call.
	*/
	void
	    SetInitializeGlFunction (InitializeGlFunction i_initialize_gl_function);


private:
	template < class RendererInterface >
	class Impl;
	utk::idiom::Pimpl< Impl< Interface > > impl_;


	/**
	   @brief The extension point to implement OpenGL initialisation procedure
	 */
	virtual void DoInitializeGl ();

	/**
	   @brief The extension point to implement OpenGL rendering
	 */
	virtual void DoPaintGl ();

	/**
	   @brief The extension point to process OpenGL viewport resizing
	 */
	virtual void DoResizeGl (int i_width, int i_height);

	void InitializeGlImpl () override final;
	void PaintGlImpl () override final;
	void ResizeGlImpl (int i_width, int i_height) override final;
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_GENERICOPENGLRENDERERBASE_HPP
