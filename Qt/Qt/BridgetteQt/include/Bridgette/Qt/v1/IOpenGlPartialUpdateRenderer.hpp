// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/IOpenGlPartialUpdateRenderer.hpp
//
// Description: An interface for classes performing OpenGL partial update
//              rendering in a manner similar to QOpenGlWindow.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_IOPENGLPARTIALUPDATERENDERER_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_IOPENGLPARTIALUPDATERENDERER_HPP


#include <utk/idiom/TypeSmartPointer.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief An interface for classes performing OpenGL partial update rendering in
   a manner similar to QOpenGlWindow
*/
class BRIDGETTEQT_EXPORT IOpenGlPartialUpdateRenderer {
public:
	UTK_IDIOM_SHARED_PTR (IOpenGlPartialUpdateRenderer)
	UTK_IDIOM_UNIQUE_PTR (IOpenGlPartialUpdateRenderer)
	UTK_IDIOM_WEAK_PTR (IOpenGlPartialUpdateRenderer)


	virtual ~IOpenGlPartialUpdateRenderer ();

	void InitializeGl ();
	void PaintGl ();
	void ResizeGl (int i_width, int i_height);

	void PaintOverGl ();
	void PaintUnderGl ();


private:
	virtual void InitializeGlImpl ();
	virtual void PaintGlImpl ();
	virtual void ResizeGlImpl (int i_width, int i_height);

	virtual void PaintOverGlImpl ();
	virtual void PaintUnderGlImpl ();
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_IOPENGLPARTIALUPDATERENDERER_HPP
