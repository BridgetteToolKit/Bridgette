// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/GenericOpenGlRendererBaseImpl.hpp
//
// Description: A generic base class for classes performing OpenGL rendering.
//              Implementation.


#ifndef QT_QT_BRIDGETTEQT_SRC_BRIDGETTE_QT_V1_GENERICOPENGLVIEWBASEIMPL_HPP
#define QT_QT_BRIDGETTEQT_SRC_BRIDGETTE_QT_V1_GENERICOPENGLVIEWBASEIMPL_HPP


#include "Bridgette/Qt/v1/GenericOpenGlRendererBase.hpp"
#include <utk/idiom/Pimpl_impl.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


template < class RendererInterface >
template < class ImplRendererInterface >
class BRIDGETTEQT_NO_EXPORT
    GenericOpenGlRendererBase< RendererInterface >::Impl {
public:
	using RendererType = GenericOpenGlRendererBase< ImplRendererInterface >;


	Impl () = default;


	bool gl_is_initialized = false;

	InitializeGlFunction initialize_gl;
	bool initialize_gl_changed = false;
};


template < class RendererInterface >
GenericOpenGlRendererBase< RendererInterface >::GenericOpenGlRendererBase () {
}


template < class RendererInterface >
GenericOpenGlRendererBase< RendererInterface >::~GenericOpenGlRendererBase () =
    default;


template < class RendererInterface >
void GenericOpenGlRendererBase<
    RendererInterface >::ResetInitializeGlFunction () {
	impl_->initialize_gl = InitializeGlFunction{};
}


template < class RendererInterface >
void GenericOpenGlRendererBase< RendererInterface >::SetInitializeGlFunction (
    InitializeGlFunction i_initialize_gl_function) {
	impl_->initialize_gl = i_initialize_gl_function;
	impl_->initialize_gl_changed = true;
}


template < class RendererInterface >
void GenericOpenGlRendererBase< RendererInterface >::DoInitializeGl () {
}


template < class RendererInterface >
void GenericOpenGlRendererBase< RendererInterface >::DoPaintGl () {
}


template < class RendererInterface >
void GenericOpenGlRendererBase< RendererInterface >::DoResizeGl (
    int /*i_width*/, int /*i_height*/) {
}


template < class RendererInterface >
void GenericOpenGlRendererBase< RendererInterface >::InitializeGlImpl () {
	DoInitializeGl ();

	if (impl_->initialize_gl) {
		impl_->initialize_gl ();
	}
}


template < class RendererInterface >
void GenericOpenGlRendererBase< RendererInterface >::PaintGlImpl () {
	if (impl_->initialize_gl_changed) {
		if (impl_->initialize_gl) {
			impl_->initialize_gl ();
		}

		impl_->initialize_gl_changed = false;
	}

	DoPaintGl ();
}


template < class RendererInterface >
void GenericOpenGlRendererBase< RendererInterface >::ResizeGlImpl (
    int i_width, int i_height) {
	DoResizeGl (i_width, i_height);
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_SRC_BRIDGETTE_QT_V1_GENERICOPENGLVIEWBASEIMPL_HPP
