// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/CameraController.hpp
//
// Description: The base class for different kinds of virtual camera or
//              viewpoint controllers.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_CAMERACONTROLLER_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_CAMERACONTROLLER_HPP


#include <QObject>

#include <QVector2D>
#include <QVector3D>

#include <utk/idiom/Pimpl.hpp>
#include <utk/idiom/TypeSmartPointer.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief The base class for camera controllers

   @details The camera controller processes the user input and emits control
   signals. These signals should be received by the object that actually changes
   the position and orientation of the camera. Not all of the signals may be
   emitted by the concrete camera controllers.

   The intent of the CameraController class hierarchy is providing a mechanism
   allowing to implement user input processing and camera orientation routines
   once and combine them to achieve the desired behaviour. For example, it
   should be possible to implement mouse event processing once and then use it
   to control camera position and orientation in different cases: Chai3D
   integration, H3D integration, Ogre3D integration, any kind of homebrew
   3D-rendering implementation.
*/
class BRIDGETTEQT_EXPORT CameraController : public QObject {
	Q_OBJECT

public:
	UTK_IDIOM_SHARED_PTR (CameraController);
	UTK_IDIOM_UNIQUE_PTR (CameraController);
	UTK_IDIOM_WEAK_PTR (CameraController);


	~CameraController ();


	// clang-format off
	/**
	   @brief The scaling coefficient applied to the user input controlling the
	   camera dolly movement

	   @details The coefficient controls the sensitivity of the user input.
	*/
	Q_PROPERTY (
		double dollyScale
		READ dollyScale
		WRITE setDollyScale
		NOTIFY dollyScaleChanged)

	/**
	   @brief The scaling coefficient applied to the user input controlling the
	   camera movement

	   @details The coefficient controls the sensitivity of the user input.
	*/
	Q_PROPERTY (
		double movementScale
		READ movementScale
		WRITE setMovementScale
		NOTIFY movementScaleChanged)

	/**
	   @brief The scaling coefficient applied to the user input controlling the
	   camera rotation

	   @details The coefficient controls the sensitivity of the user input.
	*/
	Q_PROPERTY (
		double rotationScale
		READ rotationScale
		WRITE setRotationScale
		NOTIFY rotationScaleChanged)

	/**
	   @brief The scaling coefficient applied to the user input controlling the
	   camera tracking

	   @details The coefficient controls the sensitivity of the user input.
	*/
	Q_PROPERTY (
		double trackingScale
		READ trackingScale
		WRITE setTrackingScale
		NOTIFY trackingScaleChanged)

	/**
	   @brief The scaling coefficient applied to the user input controlling the
	   camera turning

	   @details The coefficient controls the sensitivity of the user input.
	*/
	Q_PROPERTY (
		double turningScale
		READ turningScale
		WRITE setTurningScale
		NOTIFY turningScaleChanged)

	/**
	   @brief The scaling coefficient applied to the user input controlling the
	   camera zooming

	   @details The coefficient controls the sensitivity of the user input.
	*/
	Q_PROPERTY (
		double zoomScale
		READ zoomScale
		WRITE setZoomScale
		NOTIFY zoomScaleChanged)
	// clang-format on

	double dollyScale () const noexcept;
	double movementScale () const noexcept;
	double rotationScale () const noexcept;
	double trackingScale () const noexcept;
	double turningScale () const noexcept;
	double zoomScale () const noexcept;


public Q_SLOTS:
	void setDollyScale (double i_scale);
	void setMovementScale (double i_scale);
	void setRotationScale (double i_scale);
	void setTrackingScale (double i_scale);
	void setTurningScale (double i_scale);
	void setZoomScale (double i_scale);


Q_SIGNALS:
	/**
	   @brief Command to change the distance between the camera and the object

	   @param [in] i_dolly The distance change
	*/
	void Dolly (double i_dolly);

	/**
	   @brief Command to move the camera by the given vector

	   @param [in] i_move The vector of the camera movement. The
	   interpretation of the value is a part of the controlled object
	   implementation.
	*/
	void Move (QVector3D i_move);

	/**
	   @brief Command to rotate the camera around its axis

	   @param [in] i_angle The angle of the rotation in radians.
	*/
	void Rotate (double i_angle);

	/**
	   @brief Command to move the camera sideways, parallel to an object

	   @param [in] i_turn The vector of the camera position change in the plane
	   parallel to the viewport plane.
	*/
	void Track (QVector2D i_track);

	/**
	   @brief Command to turn the camera by the given direction change

	   @param [in] i_turn The vector of the camera view direction change. The
	   components of the vector represent the change of the direction mapped to
	   the screen plane. The interpretation of the value is a part of the
	   controlled object implementation.
	*/
	void Turn (QVector2D i_turn);

	/**
	   @brief Command to set the camera zoom ratio

	   @param [in] i_zoom_ratio The new value of the camera zoom ratio.
	*/
	void Zoom (double i_zoom_ratio);

	/**
	   @brief Command to change the camera zoom ratio

	   @param [in] i_zoom_ratio_change The change of the camera zoom ratio.
	*/
	void ChangeZoom (double i_zoom_ratio_change);

	void dollyScaleChanged (double i_scale);
	void movementScaleChanged (double i_scale);
	void rotationScaleChanged (double i_scale);
	void trackingScaleChanged (double i_scale);
	void turningScaleChanged (double i_scale);
	void zoomScaleChanged (double i_scale);


protected:
	CameraController ();


private:
	class Impl;
	utk::idiom::Pimpl< Impl > impl_;
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_CAMERACONTROLLER_HPP
