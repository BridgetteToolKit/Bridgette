// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name: Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/ICamera.hpp
//
// Description: The basic camera interface.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_ICAMERA_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_ICAMERA_HPP


#include <QVector3D>

#include <utk/idiom/TypeSmartPointer.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief The basic camera interface

   @note All vectors used with this and all derived classes should be in the
   OpenGL-like right-handed coordinate system.
*/
class BRIDGETTEQT_EXPORT ICamera {
public:
	UTK_IDIOM_SHARED_PTR (ICamera);
	UTK_IDIOM_UNIQUE_PTR (ICamera);
	UTK_IDIOM_WEAK_PTR (ICamera);


	virtual ~ICamera ();

	/**
	   @brief Camera field of view angle in radians

	   @returns Camera field of view angle in radians.
	*/
	double FieldOfViewAngle () const noexcept;

	/**
	   @brief Sets camera field of view angle in radians

	   @param [in] i_angle New camera field of view angle in radians.
	*/
	void SetFieldOfViewAngle (double i_angle);

	/**
	   @brief Camera view direction

	   @returns Normalised camera view direction.
	*/
	QVector3D ViewDirection () const noexcept;

	/**
	   @brief Sets camera view direction

	   @param [in] i_view_direction New camera view direction. The vector will
	   be normalised before use.
	*/
	void SetViewDirection (const QVector3D& i_view_direction);

	/**
	   @brief Camera right direction

	   @returns Normalised camera right direction vector.
	*/
	QVector3D RightDirection () const noexcept;

	/**
	   @brief Sets camera right direction

	   @param [in] i_right_direction New camera right direction vector. The
	   vector will be normalised before use.
	*/
	void SetRightDirection (const QVector3D& i_right_direction);

	/**
	   @brief Camera up direction

	   @returns Normalised camera up direction vector.
	*/
	QVector3D UpDirection () const noexcept;

	/**
	   @brief Sets camera up direction

	   @param [in] i_up_direction New camera up direction vector. The vector
	   will be normalised before use.
	*/
	void SetUpDirection (const QVector3D& i_up_direction);


private:
	virtual double FieldOfViewAngleImpl () const noexcept = 0;
	virtual void SetFieldOfViewAngleImpl (double i_angle) = 0;

	virtual QVector3D ViewDirectionImpl () const noexcept = 0;
	virtual void SetViewDirectionImpl (const QVector3D& i_view_direction) = 0;

	virtual QVector3D RightDirectionImpl () const noexcept = 0;
	virtual void SetRightDirectionImpl (const QVector3D& i_right_direction) = 0;

	virtual QVector3D UpDirectionImpl () const noexcept = 0;
	virtual void SetUpDirectionImpl (const QVector3D& i_up_direction) = 0;
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif /* QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_ICAMERA_HPP */
