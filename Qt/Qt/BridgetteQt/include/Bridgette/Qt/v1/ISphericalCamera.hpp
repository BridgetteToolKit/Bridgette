// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/ISphericalCamera.hpp
//
// Description: The spherical camera interface.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_ISPHERICALCAMERA_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_ISPHERICALCAMERA_HPP


#include <utk/idiom/TypeSmartPointer.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"

#include "Bridgette/Qt/v1/ICamera.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief The spherical camera interface
*/
class BRIDGETTEQT_EXPORT ISphericalCamera : public ICamera {
public:
	UTK_IDIOM_SHARED_PTR (ISphericalCamera);
	UTK_IDIOM_UNIQUE_PTR (ISphericalCamera);
	UTK_IDIOM_WEAK_PTR (ISphericalCamera);


	/**
	   @brief Sets the camera reference vectors

	   @param [in] The new camera origin position.

	   @param [in] The new camera azimuth direction.

	   @param [in] The new camera zenith direction.
	*/
	void SetReference (
	    const QVector3D& i_origin,
	    const QVector3D& i_azimuth,
	    const QVector3D& i_zenith);

	/**
	   @brief The camera origin position

	   @return The camera origin position.
	*/
	QVector3D Origin () const noexcept;

	/**
	   @brief Sets the camera origin position

	   @param [in] The new camera origin position.
	*/
	void SetOrigin (const QVector3D& i_origin);

	/**
	   @brief The camera azimuth direction

	   @return The camera azimuth direction.
	*/
	QVector3D Azimuth () const noexcept;

	/**
	   @brief Sets the camera azimuth direction

	   @param [in] The new camera azimuth direction.
	*/
	void SetAzimuth (const QVector3D& i_azimuth);

	/**
	   @brief The camera zenith direction

	   @return The camera zenith direction.
	*/
	QVector3D Zenith () const noexcept;

	/**
	   @brief Sets the camera zenith direction

	   @param [in] The new camera zenith direction.
	*/
	void SetZenith (const QVector3D& i_zenith);

	/**
	   @brief The camera azimuth angle

	   @returns The camera azimuth angle.
	*/
	double AzimuthAngle () const noexcept;

	/**
	   @brief Sets the camera azimuth angle

	   @param [in] i_azimuth_angle The new camera azimuth angle.
	*/
	void SetAzimuthAngle (double i_azimuth_angle);

	/**
	   @brief The camera polar angle

	   @returns The camera polar angle.
	*/
	double PolarAngle () const noexcept;

	/**
	   @brief Sets the camera polar angle

	   @param [in] i_polar_angle The new camera polar angle.
	*/
	void SetPolarAngle (double i_polar_angle);

	/**
	   @brief The camera radial distance

	   @returns The camera radial distance.
	*/
	double RadialDistance () const noexcept;

	/**
	   @brief Sets the camera radial distance

	   @param [in] i_radial_distance The new camera radial distance.
	*/
	void SetRadialDistance (double i_radial_distance);


private:
	virtual void SetReferenceImpl (
	    const QVector3D& i_origin,
	    const QVector3D& i_azimuth,
	    const QVector3D& i_zenith) = 0;

	virtual QVector3D OriginImpl () const noexcept = 0;
	virtual void SetOriginImpl (const QVector3D& i_origin) = 0;

	virtual QVector3D AzimuthImpl () const noexcept = 0;
	virtual void SetAzimuthImpl (const QVector3D& i_azimuth) = 0;

	virtual QVector3D ZenithImpl () const noexcept = 0;
	virtual void SetZenithImpl (const QVector3D& i_zenith) = 0;

	virtual double AzimuthAngleImpl () const noexcept = 0;
	virtual void SetAzimuthAngleImpl (double i_azimuth_angle) = 0;

	virtual double PolarAngleImpl () const noexcept = 0;
	virtual void SetPolarAngleImpl (double i_polar_angle) = 0;

	virtual double RadialDistanceImpl () const noexcept = 0;
	virtual void SetRadialDistanceImpl (double i_radial_distance) = 0;
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif /* QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_ISPHERICALCAMERA_HPP */
