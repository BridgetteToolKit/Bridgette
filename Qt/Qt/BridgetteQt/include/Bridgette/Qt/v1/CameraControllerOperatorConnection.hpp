// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/CameraControllerOperatorConnection.hpp
//
// Description: The convenience functions for connecting and disconnecting
//              CameraControllers and CameraOperators.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_CAMERACONTROLLEROPERATORCONNECTION_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_CAMERACONTROLLEROPERATORCONNECTION_HPP


#include <vector>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"

#include "Bridgette/Qt/v1/CameraController.hpp"
#include "Bridgette/Qt/v1/CameraOperator.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief Connects camera controller to camera operator

   @param [in] i_controller A pointer to the CameraController object that should
   be connected to the given CameraOperator object.

   @param [in] i_camera_operator A pointer to the CameraOperator object to which
   the CameraController's signals should be connected.
*/
BRIDGETTEQT_EXPORT void ConnectControllerToCameraOperator (
    CameraController* i_controller, CameraOperator* i_camera_operator);


/**
   @brief Connects camera controllers to camera operator

   @param [in] i_controllers A list of pointers to the CameraController objects
   that should be connected to the given CameraOperator object.

   @param [in] i_camera_operator A pointer to the CameraOperator object to which
   the CameraControllers' signals should be connected.
*/
BRIDGETTEQT_EXPORT void ConnectControllersToCameraOperator (
    const std::vector< CameraController* >& i_controllers,
    CameraOperator* i_camera_operator);


/**
   @brief Connects camera controller to camera operators

   @param [in] i_controller A pointer to the CameraController object that should
   be connected to the given CameraOperator objects.

   @param [in] i_camera_operators A list of pointers to the CameraOperator
   objects to which the CameraController signals should be connected.
*/
BRIDGETTEQT_EXPORT void ConnectControllerToCameraOperators (
    CameraController* i_controller,
    const std::vector< CameraOperator* >& i_camera_operators);


/**
   @brief Connects camera controllers to camera operators

   @param [in] i_controllers A list of pointers to the CameraController objects
   that should be connected to the given CameraOperator object.

   @param [in] i_camera_operators A list of pointers to the CameraOperator
   objects to which the CameraController signals should be connected.
*/
BRIDGETTEQT_EXPORT void ConnectControllersToCameraOperators (
    const std::vector< CameraController* >& i_controllers,
    const std::vector< CameraOperator* >& i_camera_operators);


/**
   @brief Disconnects camera controller from camera operator

   @param [in] i_controller A pointer to the CameraController object that should
   be disconnected from the given CameraOperator object.

   @param [in] i_camera_operator A pointer to the CameraOperator object from
   which the CameraController's signals should be disconnected.
*/
BRIDGETTEQT_EXPORT void DisconnectControllerFromCameraOperator (
    CameraController* i_controller, CameraOperator* i_camera_operator);


/**
   @brief Disconnects camera controllers from camera operator

   @param [in] i_controllers A list of pointers to the CameraController objects
   that should be disconnected from the given CameraOperator object.

   @param [in] i_camera_operator A pointer to the CameraOperator object from
   which the CameraControllers' signals should be disconnected.
*/
BRIDGETTEQT_EXPORT void DisconnectControllersFromCameraOperator (
    const std::vector< CameraController* >& i_controllers,
    CameraOperator* i_camera_operator);


/**
   @brief Disconnects camera controller from camera operators

   @param [in] i_controller A pointer to the CameraController object that should
   be disconnected from the given CameraOperator objects.

   @param [in] i_camera_operators A list of pointers to the CameraOperator
   objects from which the CameraController signals should be disconnected.
*/
BRIDGETTEQT_EXPORT void DisconnectControllerFromCameraOperators (
    CameraController* i_controller,
    const std::vector< CameraOperator* >& i_camera_operators);


/**
   @brief Disconnects camera controllers from camera operators

   @param [in] i_controllers A list of pointers to the CameraController objects
   that should be disconnected from the given CameraOperator object.

   @param [in] i_camera_operators A list of pointers to the CameraOperator
   objects from which the CameraController signals should be disconnected.
*/
BRIDGETTEQT_EXPORT void DisconnectControllersFromCameraOperators (
    const std::vector< CameraController* >& i_controllers,
    const std::vector< CameraOperator* >& i_camera_operators);


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_CAMERACONTROLLEROPERATORCONNECTION_HPP
