// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/MouseCameraController.hpp
//
// Description: The class for controlling the camera with mouse.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_MOUSECAMERACONTROLLER_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_MOUSECAMERACONTROLLER_HPP


#include <utk/idiom/Pimpl.hpp>
#include <utk/idiom/TypeSmartPointer.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"

#include "Bridgette/Qt/v1/CameraController.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief The class for controlling the camera with mouse

   @details Setting a MouseCameraController object as an event filter for
   QWidget or QWidgow based class allows controlling the cameras that are
   connected to this controller object by moving the mouse inside the UI
   component. Pressing and holding mouse buttons allows controlling camera
   turning and movement. Mouse wheel controls zoom.
*/
class BRIDGETTEQT_EXPORT MouseCameraController : public CameraController {
	Q_OBJECT

public:
	UTK_IDIOM_SHARED_PTR (MouseCameraController);
	UTK_IDIOM_UNIQUE_PTR (MouseCameraController);
	UTK_IDIOM_WEAK_PTR (MouseCameraController);

	UTK_IDIOM_MAKE_SHARED (MouseCameraController);
	UTK_IDIOM_MAKE_UNIQUE (MouseCameraController);


	MouseCameraController ();
	~MouseCameraController ();

	bool eventFilter (QObject* i_watched, QEvent* i_event) override;


private:
	class Impl;
	utk::idiom::Pimpl< Impl > impl_;
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_MOUSECAMERACONTROLLER_HPP
