// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/GenericOpenGlUiComponentBase.hpp
//
// Description: A generic base class for OpenGL viewport UI components


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_GENERICOPENGLUICOMPONENTBASE_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_GENERICOPENGLUICOMPONENTBASE_HPP


#include <type_traits>

#include <QOpenGLWidget>
#include <QOpenGLWindow>

#include <utk/idiom/Pimpl.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"

#include "Bridgette/Qt/v1/IOpenGlPartialUpdateRenderer.hpp"
#include "Bridgette/Qt/v1/IOpenGlRenderer.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief A generic base class for OpenGL viewport UI components

   @details To create a new OpenGL viewport UI component it is required to
   inherit this class and specify the desired base UI component type
   (QOpenGLWidget or QOpenGLWindow) as template argument. In case of
   QOpenGLWindow, it is also required to provide an implementation of the
   paintOverGL() and paintUnderGL() functions if they are required. These
   functions should call PaintOverGl() and PaintUnderGl() functions of the
   renderer object respectively.
*/
template < class UiComponentBaseType >
class GenericOpenGlUiComponentBase : public UiComponentBaseType {
	static_assert (
	    std::is_same< QOpenGLWidget, UiComponentBaseType >::value ||
	        std::is_same< QOpenGLWindow, UiComponentBaseType >::value,
	    "The UiComponentBaseType should be either QOpenGLWidget or "
	    "QOpenGLWindow.");

public:
	using UiComponentBase = UiComponentBaseType;
	using UiComponentType = GenericOpenGlUiComponentBase< UiComponentBase >;
	using RendererType = typename std::conditional<
	    std::is_same< QOpenGLWidget, UiComponentBase >::value,
	    IOpenGlRenderer,
	    IOpenGlPartialUpdateRenderer >::type;
	using RendererUniquePtr = typename RendererType::UniquePtr;

	template < class... Args >
	explicit GenericOpenGlUiComponentBase (Args&&... i_args);

	~GenericOpenGlUiComponentBase ();


protected:
	/**
	   @brief Returns the renderer object used to perform OpenGL rendering in
	   this viewport UI component

	   @returns Returns the renderer object used to perform OpenGL rendering in
	   this viewport UI component or nullptr if no renderer object was set.
	 */
	RendererType* Renderer () const noexcept;

	/**
	   @brief Sets the renderer object used to perform OpenGL rendering in this
	   viewport UI component

	   @param [in] i_renderer The renderer object used to perform OpenGL
	   rendering in this viewport UI component
	 */
	void SetRenderer (RendererUniquePtr i_renderer);

	// QOpenGLWidget QOpenGLWindow
	void initializeGL () override;
	void paintGL () override;
	void resizeGL (int i_width, int i_height) override;


private:
	template < class UiComponentBaseType >
	class Impl;
	utk::idiom::Pimpl< Impl< UiComponentBase > > impl_;
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_GENERICOPENGLUICOMPONENTBASE_HPP
