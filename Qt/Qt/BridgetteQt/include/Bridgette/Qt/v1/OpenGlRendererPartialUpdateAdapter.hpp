// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/OpenGlRendererPartialUpdateAdapter.hpp
//
// Description: An adapter for classes implementing IOpenGlRenderer interface to
//              enable using them where the classes implementing the
//              IOpenGlPartialUpdateRenderer interface are required.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_OPENGLRENDERERPARTIALUPDATEADAPTER_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_OPENGLRENDERERPARTIALUPDATEADAPTER_HPP


#include <utk/idiom/Pimpl.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"

#include "Bridgette/Qt/v1/IOpenGlPartialUpdateRenderer.hpp"
#include "Bridgette/Qt/v1/IOpenGlRenderer.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief An adapter for classes implementing IOpenGlRenderer interface to
   enable using them where the classes implementing the
   IOpenGlPartialUpdateRenderer interface are required.
*/
class BRIDGETTEQT_EXPORT OpenGlRendererPartialUpdateAdapter
    : public IOpenGlPartialUpdateRenderer {
public:
	OpenGlRendererPartialUpdateAdapter (IOpenGlRenderer::UniquePtr i_renderer);

	virtual ~OpenGlRendererPartialUpdateAdapter ();

	IOpenGlRenderer* Renderer () const noexcept;


private:
	class Impl;
	utk::idiom::Pimpl< Impl > impl_;

	void InitializeGlImpl () override;
	void PaintGlImpl () override;
	void ResizeGlImpl (int i_width, int i_height) override;
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_OPENGLRENDERERPARTIALUPDATEADAPTER_HPP
