// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/IOpenGlRenderer.hpp
//
// Description: An interface for classes performing OpenGL rendering in a manner
//              similar to QOpenGlWidget and QOpenGlWindow.


#ifndef QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_IOPENGLRENDERER_HPP
#define QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_IOPENGLRENDERER_HPP


#include <utk/idiom/TypeSmartPointer.hpp>

#include "Bridgette/Qt/BridgetteQt_export.h"
#include "Bridgette/Qt/v1/namespace.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


/**
   @brief An interface for classes performing OpenGL rendering in a manner
   similar to QOpenGlWidget and QOpenGlWindow
*/
class BRIDGETTEQT_EXPORT IOpenGlRenderer {
public:
	UTK_IDIOM_SHARED_PTR (IOpenGlRenderer)
	UTK_IDIOM_UNIQUE_PTR (IOpenGlRenderer)
	UTK_IDIOM_WEAK_PTR (IOpenGlRenderer)


	virtual ~IOpenGlRenderer ();

	void InitializeGl ();
	void PaintGl ();
	void ResizeGl (int i_width, int i_height);


private:
	virtual void InitializeGlImpl ();
	virtual void PaintGlImpl ();
	virtual void ResizeGlImpl (int i_width, int i_height);
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif /* QT_QT_BRIDGETTEQT_INCLUDE_BRIDGETTE_QT_V1_IOPENGLRENDERER_HPP */
