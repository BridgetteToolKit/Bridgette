// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/CameraOperatorImpl.cpp
//
// Description: The base class for camera operators, controlling virtual cameras
//              or viewpoints. PIMPL class implementation.


#include "CameraOperatorImpl.hpp"


#include <algorithm>

#include <boost/math/constants/constants.hpp>


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


CameraOperator::Impl::Impl () = default;


void CameraOperator::Impl::ApplyZoom (double i_zoom_ratio) {
	zoom_ratio = std::max (0.0, i_zoom_ratio);

	camera->SetFieldOfViewAngle (std::min (
	    boost::math::constants::pi< double > (),
	    original_camera_fov / zoom_ratio));
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE
