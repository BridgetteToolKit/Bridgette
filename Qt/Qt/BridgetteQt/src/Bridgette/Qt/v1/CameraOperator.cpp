// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/CameraOperator.cpp
//
// Description: The base class for camera operators, controlling virtual cameras
//              or viewpoints. Interface class implementation.


#include "CameraOperatorImpl.hpp"
#include <utk/idiom/Pimpl_impl.hpp>


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


CameraOperator::CameraOperator (ICamera::SharedPtr i_camera) {
	SetCamera (i_camera);
}


CameraOperator::~CameraOperator () = default;


void CameraOperator::Dolly (double i_dolly) {
	if (!impl_->camera) {
		return;
	}

	DollyImpl (i_dolly);
}


void CameraOperator::Move (QVector3D i_move) {
	if (!impl_->camera) {
		return;
	}

	MoveImpl (i_move);
}


void CameraOperator::Rotate (double i_angle) {
	if (!impl_->camera) {
		return;
	}

	RotateImpl (i_angle);
}


void CameraOperator::Track (QVector2D i_track) {
	if (!impl_->camera) {
		return;
	}

	TrackImpl (i_track);
}


void CameraOperator::Turn (QVector2D i_turn) {
	if (!impl_->camera) {
		return;
	}

	TurnImpl (i_turn);
}


void CameraOperator::Zoom (double i_zoom_ratio) {
	if (!impl_->camera) {
		return;
	}

	ZoomImpl (i_zoom_ratio);
}


void CameraOperator::ChangeZoom (double i_zoom_ratio_change) {
	if (!impl_->camera) {
		return;
	}

	ChangeZoomImpl (i_zoom_ratio_change);
}


ICamera* CameraOperator::Camera () const noexcept {
	return impl_->camera.get ();
}


void CameraOperator::SetCamera (ICamera::SharedPtr i_camera, bool i_keep_zoom) {
	impl_->camera = std::move (i_camera);

	if (!impl_->camera) {
		return;
	}

	impl_->original_camera_fov = impl_->camera->FieldOfViewAngle ();

	if (i_keep_zoom) {
		impl_->camera->SetFieldOfViewAngle (
		    impl_->original_camera_fov / impl_->zoom_ratio);
	}
	else {
		impl_->zoom_ratio = 1.0;
	}
}


void CameraOperator::ZoomImpl (double i_zoom_ratio) {
	impl_->ApplyZoom (i_zoom_ratio);
}


void CameraOperator::ChangeZoomImpl (double i_zoom_ratio_change) {
	impl_->ApplyZoom (impl_->zoom_ratio + i_zoom_ratio_change);
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#include "Bridgette/Qt/v1/moc_CameraOperator.cpp"
