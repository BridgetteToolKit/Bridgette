// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/CameraControllerProperties.cpp
//
// Description: The base class for different kinds of virtual camera or
//              viewpoint controllers. Qt properties access methods
//              implementation.


#include "CameraControllerImpl.hpp"
#include <utk/idiom/Pimpl_impl.hpp>


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


double CameraController::dollyScale () const noexcept {
	return impl_->dolly_scale;
}


void CameraController::setDollyScale (double i_dolly_scale) {
	if (i_dolly_scale == impl_->dolly_scale) {
		return;
	}

	impl_->dolly_scale = i_dolly_scale;

	Q_EMIT dollyScaleChanged (i_dolly_scale);
}


double CameraController::movementScale () const noexcept {
	return impl_->movement_scale;
}


void CameraController::setMovementScale (double i_movement_scale) {
	if (i_movement_scale == impl_->movement_scale) {
		return;
	}

	impl_->movement_scale = i_movement_scale;

	Q_EMIT movementScaleChanged (i_movement_scale);
}


double CameraController::rotationScale () const noexcept {
	return impl_->rotation_scale;
}


void CameraController::setRotationScale (double i_rotation_scale) {
	if (i_rotation_scale == impl_->rotation_scale) {
		return;
	}

	impl_->rotation_scale = i_rotation_scale;

	Q_EMIT rotationScaleChanged (i_rotation_scale);
}


double CameraController::trackingScale () const noexcept {
	return impl_->tracking_scale;
}


void CameraController::setTrackingScale (double i_tracking_scale) {
	if (i_tracking_scale == impl_->tracking_scale) {
		return;
	}

	impl_->tracking_scale = i_tracking_scale;

	Q_EMIT trackingScaleChanged (i_tracking_scale);
}


double CameraController::turningScale () const noexcept {
	return impl_->turning_scale;
}


void CameraController::setTurningScale (double i_turning_scale) {
	if (i_turning_scale == impl_->turning_scale) {
		return;
	}

	impl_->turning_scale = i_turning_scale;

	Q_EMIT turningScaleChanged (i_turning_scale);
}


double CameraController::zoomScale () const noexcept {
	return impl_->zoom_scale;
}


void CameraController::setZoomScale (double i_zoom_scale) {
	if (i_zoom_scale == impl_->zoom_scale) {
		return;
	}

	impl_->zoom_scale = i_zoom_scale;

	Q_EMIT zoomScaleChanged (i_zoom_scale);
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE
