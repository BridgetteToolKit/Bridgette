// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/MouseCameraControllerImpl.hpp
//
// Description: The class for controlling the camera with mouse. PIMPL class
//              declaration.


#ifndef QT_QT_BRIDGETTEQT_SRC_BRIDGETTE_QT_V1_MOUSECAMERACONTROLLERIMPL_HPP
#define QT_QT_BRIDGETTEQT_SRC_BRIDGETTE_QT_V1_MOUSECAMERACONTROLLERIMPL_HPP


#include "Bridgette/Qt/v1/MouseCameraController.hpp"

#include <QMouseEvent>
#include <QPointF>
#include <QWheelEvent>


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


class BRIDGETTEQT_NO_EXPORT MouseCameraController::Impl {
public:
	explicit Impl (MouseCameraController* i_owner);

	bool MouseButtonPress (QMouseEvent* i_event);
	bool MouseButtonRelease (QMouseEvent* i_event);
	bool MouseMove (QMouseEvent* i_event);

	bool Wheel (QWheelEvent* i_event);


private:
	/**
	   @internal

	   @brief A mask for all mouse buttons used to control the camera
	*/
	static constexpr int kControlButtons =
	    int(Qt::LeftButton) | int(Qt::MidButton) | int(Qt::RightButton);

	/**
	   @internal

	   @brief A mask for mouse buttons used to control the camera movement in
	   horizontal plane (XZ)
	*/
	static constexpr int kPlanarMovementControllButtons =
	    int(Qt::LeftButton) | int(Qt::RightButton);

	/**
	   @internal

	   @brief A mask for mouse buttons used to control the camera movement along
	   vertial axis (Y)
	*/
	static constexpr int kVerticalMovementControllButtons =
	    int(Qt::RightButton) | int(Qt::MidButton);

	/**
	   @internal

	   @brief A mask for mouse buttons used to control the camera rotation
	   around its view direction vector
	*/
	static constexpr int kRotationControllButtons =
	    int(Qt::LeftButton) | int(Qt::MidButton);


	MouseCameraController* owner_;

	bool mouse_is_moving_with_pressed_buttons = false;

	QPointF last_mouse_position;
};


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#endif // QT_QT_BRIDGETTEQT_SRC_BRIDGETTE_QT_V1_MOUSECAMERACONTROLLERIMPL_HPP
