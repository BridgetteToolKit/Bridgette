// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/SphericalCameraOperator.cpp
//
// Description: The camera operator that operates in spherical coordinates.
//              Interface class implementation.


#include "SphericalCameraOperatorImpl.hpp"
#include <utk/idiom/Pimpl_impl.hpp>


#include <boost/math/constants/constants.hpp>

#include <QQuaternion>

BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


SphericalCameraOperator::SphericalCameraOperator (
    ISphericalCamera::SharedPtr i_camera)
    : CameraOperator (std::move (i_camera)) {
}


SphericalCameraOperator::~SphericalCameraOperator () = default;


ISphericalCamera* SphericalCameraOperator::Camera () const noexcept {
	return static_cast< ISphericalCamera* > (CameraOperator::Camera ());
}


void SphericalCameraOperator::SetCamera (
    ISphericalCamera::SharedPtr i_camera, bool i_keep_zoom) {
	CameraOperator::SetCamera (i_camera, i_keep_zoom);
}


void SphericalCameraOperator::DollyImpl (double i_dolly) {
	Camera ()->SetRadialDistance (
	    std::max (0.0, Camera ()->RadialDistance () + i_dolly));
}


void SphericalCameraOperator::MoveImpl (QVector3D i_move) {
	Camera ()->SetOrigin (Camera ()->Origin () + i_move);
}


void SphericalCameraOperator::RotateImpl (double i_angle) {
	auto const rotation_quaternion = QQuaternion::fromAxisAndAngle (
	    Camera ()->ViewDirection (),
	    boost::math::constants::radian< double > () * i_angle);

	Camera ()->SetAzimuth (
	    rotation_quaternion.rotatedVector (Camera ()->Azimuth ()));

	Camera ()->SetZenith (
	    rotation_quaternion.rotatedVector (Camera ()->Zenith ()));
}


void SphericalCameraOperator::TrackImpl (QVector2D i_track) {
	Camera ()->SetOrigin (
	    Camera ()->Origin () +
	    (Camera ()->RightDirection () * i_track.x () +
	     Camera ()->UpDirection () * i_track.y ()));
}


void SphericalCameraOperator::TurnImpl (QVector2D i_turn) {
	Camera ()->SetAzimuthAngle (Camera ()->AzimuthAngle () + i_turn.x ());
	Camera ()->SetPolarAngle (Camera ()->PolarAngle () + i_turn.y ());
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#include "Bridgette/Qt/v1/moc_SphericalCameraOperator.cpp"
