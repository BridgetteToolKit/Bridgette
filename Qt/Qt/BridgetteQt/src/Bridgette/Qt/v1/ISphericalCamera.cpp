// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/ISphericalCamera.cpp
//
// Description: The spherical camera interface.. Implementation.


#include "Bridgette/Qt/v1/ISphericalCamera.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


void ISphericalCamera::SetReference (
    const QVector3D& i_origin,
    const QVector3D& i_azimuth,
    const QVector3D& i_zenith) {
}


QVector3D ISphericalCamera::Origin () const noexcept {
	return OriginImpl ();
}


void ISphericalCamera::SetOrigin (const QVector3D& i_origin) {
	SetOriginImpl (i_origin);
}


QVector3D ISphericalCamera::Azimuth () const noexcept {
	return AzimuthImpl ();
}


void ISphericalCamera::SetAzimuth (const QVector3D& i_azimuth) {
	SetAzimuthImpl (i_azimuth);
}


QVector3D ISphericalCamera::Zenith () const noexcept {
	return ZenithImpl ();
}


void ISphericalCamera::SetZenith (const QVector3D& i_zenith) {
	SetZenithImpl (i_zenith);
}


double ISphericalCamera::AzimuthAngle () const noexcept {
	return AzimuthAngleImpl ();
}


void ISphericalCamera::SetAzimuthAngle (double i_azimuth_angle) {
	SetAzimuthAngleImpl (i_azimuth_angle);
}


double ISphericalCamera::PolarAngle () const noexcept {
	return PolarAngleImpl ();
}


void ISphericalCamera::SetPolarAngle (double i_polar_angle) {
	SetPolarAngleImpl (i_polar_angle);
}


double ISphericalCamera::RadialDistance () const noexcept {
	return RadialDistanceImpl ();
}


void ISphericalCamera::SetRadialDistance (double i_radial_distance) {
	SetRadialDistanceImpl (i_radial_distance);
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE
