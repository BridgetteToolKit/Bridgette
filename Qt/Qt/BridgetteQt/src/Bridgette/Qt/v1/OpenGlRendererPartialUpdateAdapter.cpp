// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/OpenGlRendererPartialUpdateAdapter.cpp
//
// Description: An adapter for classes implementing IOpenGlRenderer interface to
//              enable using them where the classes implementing the
//              IOpenGlPartialUpdateRenderer interface are required.
//              Interface class implementation.


#include "OpenGlRendererPartialUpdateAdapterImpl.hpp"
#include <utk/idiom/Pimpl_impl.hpp>


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


OpenGlRendererPartialUpdateAdapter::OpenGlRendererPartialUpdateAdapter (
    IOpenGlRenderer::UniquePtr i_renderer)
    : impl_{std::move (i_renderer)} {
}


OpenGlRendererPartialUpdateAdapter::~OpenGlRendererPartialUpdateAdapter () =
    default;


IOpenGlRenderer*
    OpenGlRendererPartialUpdateAdapter::Renderer () const noexcept {
	return impl_->renderer.get ();
}


void OpenGlRendererPartialUpdateAdapter::InitializeGlImpl () {
	if (impl_->renderer) {
		impl_->renderer->InitializeGl ();
	}
}


void OpenGlRendererPartialUpdateAdapter::PaintGlImpl () {
	if (impl_->renderer) {
		impl_->renderer->PaintGl ();
	}
}


void OpenGlRendererPartialUpdateAdapter::ResizeGlImpl (
    int i_width, int i_height) {
	if (impl_->renderer) {
		impl_->renderer->ResizeGl (i_width, i_height);
	}
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE
