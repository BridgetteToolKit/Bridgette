# Copyright 2018 Bridgette Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy of
# the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#
#
# File name: Bridgette/Qt/Qt/BridgetteQt/scr/Bridgette/Qt/v1/CMakeLists.txt
#
# Description: Bridgette Qt Integration Library sources listing.


set (SOURCES
  CameraController.cpp
  CameraControllerImpl.cpp
  CameraControllerImpl.hpp
  CameraControllerOperatorConnection.cpp
  CameraControllerProperties.cpp
  CameraOperator.cpp
  CameraOperatorImpl.cpp
  CameraOperatorImpl.hpp
  ICamera.cpp
  IOpenGlPartialUpdateRenderer.cpp
  IOpenGlRenderer.cpp
  ISphericalCamera.cpp
  MouseCameraController.cpp
  MouseCameraControllerImpl.cpp
  MouseCameraControllerImpl.hpp
  OpenGlPartialUpdateRendererBase.cpp
  OpenGlRendererBase.cpp
  OpenGlRendererPartialUpdateAdapter.cpp
  OpenGlRendererPartialUpdateAdapterImpl.cpp
  OpenGlRendererPartialUpdateAdapterImpl.hpp
  OpenGlWidget.cpp
  OpenGlWidgetImpl.cpp
  OpenGlWidgetImpl.hpp
  OpenGlWindow.cpp
  OpenGlWindowImpl.cpp
  OpenGlWindowImpl.hpp
  SphericalCameraOperator.cpp
  SphericalCameraOperatorImpl.cpp
  SphericalCameraOperatorImpl.hpp
  )


file(RELATIVE_PATH PREFIX
  ${PROJECT_SOURCE_DIR}
  ${CMAKE_CURRENT_LIST_DIR})


foreach (SOURCE IN LISTS SOURCES)
  utk_cmake_target_sources (
    TARGET ${${PROJECT_NAME}_TARGET_LIST}
    PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/${SOURCE}
    )
endforeach (SOURCE IN SOURCES)
