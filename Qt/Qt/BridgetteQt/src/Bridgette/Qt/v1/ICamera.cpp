// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name: Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/ICamera.cpp
//
// Description: The basic camera interface. Implementation.


#include "Bridgette/Qt/v1/ICamera.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


ICamera::~ICamera () = default;


double ICamera::FieldOfViewAngle () const noexcept {
	return FieldOfViewAngleImpl ();
}


void ICamera::SetFieldOfViewAngle (double i_angle) {
	SetFieldOfViewAngleImpl (i_angle);
}


QVector3D ICamera::ViewDirection () const noexcept {
	return ViewDirectionImpl ();
}


void ICamera::SetViewDirection (const QVector3D& i_view_direction) {
	SetViewDirectionImpl (i_view_direction);
}


QVector3D ICamera::RightDirection () const noexcept {
	return RightDirectionImpl ();
}


void ICamera::SetRightDirection (const QVector3D& i_right_direction) {
	SetRightDirectionImpl (i_right_direction);
}


QVector3D ICamera::UpDirection () const noexcept {
	return UpDirectionImpl ();
}


void ICamera::SetUpDirection (const QVector3D& i_up_direction) {
	SetUpDirectionImpl (i_up_direction);
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE
