// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/MouseCameraControllerImpl.cpp
//
// Description: The class for controlling the camera with mouse. PIMPL class
//              implementation.


#include "MouseCameraControllerImpl.hpp"


#include <boost/math/constants/constants.hpp>
#include <cmath>


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


MouseCameraController::Impl::Impl (MouseCameraController* i_owner)
    : owner_{i_owner} {
}


bool MouseCameraController::Impl::MouseButtonPress (QMouseEvent* i_event) {
	bool event_processed = false;

	/*
	  Only process events that do not start double click event and are caused by
	  one of the control mouse buttons. Other events may be processed by some
	  other objects.
	 */
	if (!(Qt::MouseEventCreatedDoubleClick & i_event->flags ()) &&
	    (kControlButtons & i_event->button ())) {
		mouse_is_moving_with_pressed_buttons = true;

		last_mouse_position = i_event->localPos ();

		i_event->accept ();

		event_processed = true;
	}

	return event_processed;
}


bool MouseCameraController::Impl::MouseButtonRelease (QMouseEvent* i_event) {
	bool event_processed = false;

	/*
	  Only process events that are caused by one of the control mouse buttons.
	  Other events may be processed by some other objects.
	*/
	if (kControlButtons & i_event->button ()) {
		if (!(kControlButtons & i_event->buttons ())) {
			mouse_is_moving_with_pressed_buttons = false;
		}

		i_event->accept ();

		event_processed = true;
	}

	return event_processed;
}


bool MouseCameraController::Impl::MouseMove (QMouseEvent* i_event) {
	if (!mouse_is_moving_with_pressed_buttons) {
		return false;
	}

	auto const event_control_buttons = kControlButtons & i_event->buttons ();

	const QVector2D mouse_move_vector{i_event->localPos () -
	                                  last_mouse_position};

	auto const current_mouse_position{i_event->localPos ()};

	switch (event_control_buttons) {
	default: {
	} break;

	case Qt::LeftButton: {
		Q_EMIT owner_->Turn (
		    boost::math::constants::degree< double > () *
		    owner_->turningScale () * mouse_move_vector);
	} break;

	case Qt::MidButton: {
		const double dolly =
		    (current_mouse_position.y () - last_mouse_position.y ());

		Q_EMIT owner_->Dolly (owner_->dollyScale () * dolly);
	} break;

	case Qt::RightButton: {
		/*
		  Y-axis direction in widgets is downwards.
		*/
		const QVector2D camera_track_vector{mouse_move_vector.x (),
		                                    -mouse_move_vector.y ()};

		Q_EMIT owner_->Track (owner_->trackingScale () * camera_track_vector);
	} break;

	case kRotationControllButtons: {
		const double rotation_angle = owner_->rotationScale () *
		    (current_mouse_position.x () - last_mouse_position.x ());

		/*
		  The mouse moves at list 1 pixel a time. Treating 1 as radian will
		  result in too fast rotation with the default rotation scale of 1.0.
		*/
		Q_EMIT owner_->Rotate (
		    boost::math::constants::degree< double > () * rotation_angle);
	} break;

	case kPlanarMovementControllButtons: {
		/*
		  Y-axis direction in widgets is downwards.
		*/
		const QVector3D camera_move_vector{
		    mouse_move_vector.x (), 0.0, mouse_move_vector.y ()};

		Q_EMIT owner_->Move (owner_->movementScale () * camera_move_vector);
	} break;

	case kVerticalMovementControllButtons: {
		/*
		  Y-axis direction in widgets is downwards.
		*/
		const QVector3D camera_move_vector{0.0, -mouse_move_vector.y (), 0.0};

		Q_EMIT owner_->Move (owner_->movementScale () * camera_move_vector);
	} break;
	}

	last_mouse_position = i_event->localPos ();

	i_event->accept ();

	return true;
}


bool MouseCameraController::Impl::Wheel (QWheelEvent* i_event) {
	/*
	  The angle delta is in eights of a degree.
	*/
	QPoint rotation_angle = i_event->angleDelta () / 8;

	if (!rotation_angle.isNull ()) {
		/*
		  Most mouse types work in steps of 15 degrees. Devision gives a value
		  that is easier to scale that the value in degrees. For mice with
		  fined-resolution wheels this value may be in range (0, 1), for other -
		  in range [1, +inf).
		*/
		double zoom_ratio_change = double(rotation_angle.y ()) / 15.0;

		Q_EMIT owner_->ChangeZoom (owner_->zoomScale () * zoom_ratio_change);
	}

	i_event->accept ();

	return true;
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE
