// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/OpenGlWindow.cpp
//
// Description: The window class for OpenGL rendering performed by the software
//              that is being integrated with Qt. Interface class
//              implementation.


#include "OpenGlWindowImpl.hpp"
#include <utk/idiom/Pimpl_impl.hpp>

#include "Bridgette/Qt/v1/GenericOpenGlUiComponentBaseImpl.hpp"


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


OpenGlWindow::OpenGlWindow (
    QOpenGLWindow::UpdateBehavior i_update_behavior, QWindow* i_parent)
    : Base (i_update_behavior, i_parent) {
}


OpenGlWindow::OpenGlWindow (
    QOpenGLContext* i_share_context,
    QOpenGLWindow::UpdateBehavior i_update_behavior,
    QWindow* i_parent)
    : Base (i_share_context, i_update_behavior, i_parent) {
}


OpenGlWindow::~OpenGlWindow () = default;


IOpenGlPartialUpdateRenderer* OpenGlWindow::Renderer () const {
	return Base::Renderer ();
}


void OpenGlWindow::SetRenderer (
    IOpenGlPartialUpdateRenderer::UniquePtr i_opengl_renderer) {
	Base::SetRenderer (std::move (i_opengl_renderer));
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE
