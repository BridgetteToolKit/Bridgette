// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/include/Bridgette/Qt/v1/CameraControllerOperatorConnection.hpp
//
// Description: The convenience functions for connecting and disconnecting
//              CameraControllers and CameraOperators. Implementation.


#include "Bridgette/Qt/v1/CameraControllerOperatorConnection.hpp"

#include <QObject>


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


void ConnectControllerToCameraOperator (
    CameraController* i_controller, CameraOperator* i_camera_operator) {
	ConnectControllersToCameraOperators ({i_controller}, {i_camera_operator});
}


void ConnectControllersToCameraOperator (
    const std::vector< CameraController* >& i_controllers,
    CameraOperator* i_camera_operator) {
	ConnectControllersToCameraOperators (i_controllers, {i_camera_operator});
}


void ConnectControllerToCameraOperators (
    CameraController* i_controller,
    const std::vector< CameraOperator* >& i_camera_operators) {
	ConnectControllersToCameraOperators ({i_controller}, i_camera_operators);
}


void ConnectControllersToCameraOperators (
    const std::vector< CameraController* >& i_controllers,
    const std::vector< CameraOperator* >& i_camera_operators) {
	for (auto const controller : i_controllers) {
		if (!controller) {
			continue;
		}

		for (auto const camera_operator : i_camera_operators) {
			if (!camera_operator) {
				continue;
			}

			QObject::connect (
			    controller,
			    &CameraController::Dolly,
			    camera_operator,
			    &CameraOperator::Dolly);

			QObject::connect (
			    controller,
			    &CameraController::Move,
			    camera_operator,
			    &CameraOperator::Move);

			QObject::connect (
			    controller,
			    &CameraController::Rotate,
			    camera_operator,
			    &CameraOperator::Rotate);

			QObject::connect (
			    controller,
			    &CameraController::Track,
			    camera_operator,
			    &CameraOperator::Track);

			QObject::connect (
			    controller,
			    &CameraController::Turn,
			    camera_operator,
			    &CameraOperator::Turn);

			QObject::connect (
			    controller,
			    &CameraController::Zoom,
			    camera_operator,
			    &CameraOperator::Zoom);

			QObject::connect (
			    controller,
			    &CameraController::ChangeZoom,
			    camera_operator,
			    &CameraOperator::ChangeZoom);
		}
	}
}


void DisconnectControllerFromCameraOperator (
    CameraController* i_controller, CameraOperator* i_camera_operator) {
	DisconnectControllersFromCameraOperators (
	    {i_controller}, {i_camera_operator});
}


void DisconnectControllersFromCameraOperator (
    const std::vector< CameraController* >& i_controllers,
    CameraOperator* i_camera_operator) {
	DisconnectControllersFromCameraOperators (
	    i_controllers, {i_camera_operator});
}


void DisconnectControllerFromCameraOperators (
    CameraController* i_controller,
    const std::vector< CameraOperator* >& i_camera_operators) {
	DisconnectControllersFromCameraOperators (
	    {i_controller}, i_camera_operators);
}


void DisconnectControllersFromCameraOperators (
    const std::vector< CameraController* >& i_controllers,
    const std::vector< CameraOperator* >& i_camera_operators) {
	for (auto const controller : i_controllers) {
		if (!controller) {
			continue;
		}

		for (auto const camera_operator : i_camera_operators) {
			if (!camera_operator) {
				continue;
			}

			QObject::disconnect (
			    controller,
			    &CameraController::Dolly,
			    camera_operator,
			    &CameraOperator::Dolly);

			QObject::disconnect (
			    controller,
			    &CameraController::Move,
			    camera_operator,
			    &CameraOperator::Move);

			QObject::disconnect (
			    controller,
			    &CameraController::Rotate,
			    camera_operator,
			    &CameraOperator::Rotate);

			QObject::disconnect (
			    controller,
			    &CameraController::Track,
			    camera_operator,
			    &CameraOperator::Track);

			QObject::disconnect (
			    controller,
			    &CameraController::Turn,
			    camera_operator,
			    &CameraOperator::Turn);

			QObject::disconnect (
			    controller,
			    &CameraController::Zoom,
			    camera_operator,
			    &CameraOperator::Zoom);

			QObject::disconnect (
			    controller,
			    &CameraController::ChangeZoom,
			    camera_operator,
			    &CameraOperator::ChangeZoom);
		}
	}
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE
