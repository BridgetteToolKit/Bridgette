// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Qt/Qt/BridgetteQt/src/Bridgette/Qt/v1/MouseCameraController.cpp
//
// Description: The class for controlling the camera with mouse. Interface class
//              implementation.


#include "MouseCameraControllerImpl.hpp"
#include <utk/idiom/Pimpl_impl.hpp>

#include <QEvent>
#include <QMouseEvent>
#include <QWheelEvent>


BRIDGETTE_QT_QT_V1_OPEN_NAMESPACE


MouseCameraController::MouseCameraController ()
    : impl_{this} {
}


MouseCameraController::~MouseCameraController () = default;


bool MouseCameraController::eventFilter (QObject* i_watched, QEvent* i_event) {
	if (!i_event) {
		return false;
	}

	bool event_processed = false;

	switch (i_event->type ()) {
	default: { event_processed = false; } break;

	case QEvent::MouseButtonPress: {
		event_processed =
		    impl_->MouseButtonPress (static_cast< QMouseEvent* > (i_event));
	} break;

	case QEvent::MouseButtonRelease: {
		event_processed =
		    impl_->MouseButtonRelease (static_cast< QMouseEvent* > (i_event));
	} break;

	case QEvent::MouseMove: {
		event_processed =
		    impl_->MouseMove (static_cast< QMouseEvent* > (i_event));
	} break;

	case QEvent::Wheel: {
		event_processed = impl_->Wheel (static_cast< QWheelEvent* > (i_event));
	} break;
	}

	return event_processed;
}


BRIDGETTE_QT_QT_V1_CLOSE_NAMESPACE


#include "Bridgette/Qt/v1/moc_MouseCameraController.cpp"
