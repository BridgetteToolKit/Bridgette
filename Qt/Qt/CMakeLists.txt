# Copyright 2018 Bridgette Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy of
# the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#
#
# File name: Bridgette/Qt/Qt/CMakeLists.txt
#
# Description: Bridgette Qt Integration Library root project.

cmake_minimum_required (VERSION 3.3 FATAL_ERROR)

set (PARENT_PROJECT ${PROJECT_NAME})
set (PARENT_PROJECT_DIR "${${PROJECT_NAME}_DIRECTORY}")

project (BridgetteQt)

set (${PROJECT_NAME}_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}")


#############################
# Build and install options #
#############################
utk_cmake_build_and_install_options (
  INSTALL_DEVEL
  SHARED_LIBRARY
  SHARED_LIBRARY_ENABLED
  STATIC_LIBRARY
  TESTS
  )


###########################
# BridgetteQt project #
###########################
set (PROJECT_FOLDER "${PARENT_PROJECT}/Qt")

add_subdirectory (BridgetteQt)


####################
# Optional targets #
####################
if (${PROJECT_NAME}_BUILD_TESTS)
  set (${PARENT_PROJECT}_ENABLE_TESTING  TRUE  PARENT_SCOPE)

  add_subdirectory (unit_tests)
endif ()
