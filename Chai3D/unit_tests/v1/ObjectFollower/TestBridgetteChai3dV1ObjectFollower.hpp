// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name: Bridgette/Chai3D/unit_tests/v1/ObjectFollower/TestBridgetteChai3dV1ObjectFollower.hpp
//
// Description: Generic object follower class unit test.


#ifndef CHAI3D_TESTS_V1_TOOL_TESTBRIDGETTECHAI3DV1TOOL_HPP
#define CHAI3D_TESTS_V1_TOOL_TESTBRIDGETTECHAI3DV1TOOL_HPP


#include <memory>

#include <gtest/gtest.h>

#include <display/CCamera.h>
#include <tools/CGenericTool.h>
#include <world/CWorld.h>

#include <Bridgette/Chai3D/ObjectFollower.hpp>


template < class FollowerType >
using CameraFollower =
    bridgette::chai3d::ObjectFollower< FollowerType, ::chai3d::cCamera >;

using ToolCameraFollower = CameraFollower< ::chai3d::cGenericTool >;


TEST (TestBridgetteChai3dV1Tool, FollowObject) {
	auto world = std::make_unique< ::chai3d::cWorld > ();

	auto tool = ToolCameraFollower::MakeUnique (world.get ());

	// Not following any camera
	ASSERT_FALSE (tool->IsFollowingAnyObject ())
	    << "The tool was not set to follow any camera";

	ASSERT_FALSE (tool->IsFollowingObject (nullptr))
	    << "The tool cannot follow nullptr camera";

	auto first_camera = std::make_shared< ::chai3d::cCamera > (world.get ());
	auto second_camera = std::make_shared< ::chai3d::cCamera > (world.get ());

	// Follow first camera
	tool->FollowObject (first_camera);

	ASSERT_TRUE (tool->IsFollowingAnyObject ())
	    << "The tool was set to follow camera";

	ASSERT_TRUE (tool->IsFollowingObject (first_camera.get ()))
	    << "The tool was set to follow camera #" << first_camera.get ();

	ASSERT_FALSE (tool->IsFollowingObject (second_camera.get ()))
	    << "The tool was not set to follow camera #" << second_camera.get ();

	// Follow second camera
	tool->FollowObject (second_camera);

	ASSERT_TRUE (tool->IsFollowingAnyObject ())
	    << "The tool was set to follow camera";

	ASSERT_TRUE (tool->IsFollowingObject (second_camera.get ()))
	    << "The tool was set to follow camera #" << second_camera.get ();

	ASSERT_FALSE (tool->IsFollowingObject (first_camera.get ()))
	    << "The tool was not set to follow camera #" << first_camera.get ();

	// Stop following camera
	tool->StopFollowingObject ();

	ASSERT_FALSE (tool->IsFollowingAnyObject ())
	    << "The tool was set to follow no camera";
}


TEST (TestBridgetteChai3dV1Tool, SyncLocalRotation) {
	auto world = std::make_unique< ::chai3d::cWorld > ();

	auto tool = ToolCameraFollower::MakeUnique (world.get ());

	auto const tool_initial_rotation =
	    ::chai3d::cMatrix3d (0, 1, 2, 3, 4, 5, 6, 7, 8);

	tool->setLocalRot (tool_initial_rotation);

	// Sycnhronise local rotation without following any object.
	tool->SyncLocalRotation ();

	/*
	  String representation comparison is used because it will provide a more
	  readable result and ::chai3d::cMatrix3d does not provide operator==.
	  ::chai3d::cMatrix3d::equals() method takes non-constant reference that
	  makes it impossible to compare with constant object.
	*/
	ASSERT_EQ (tool->getLocalRot ().str (), tool_initial_rotation.str ());

	// Sycnhronise local rotation while following camera.
	auto camera = std::make_shared< ::chai3d::cCamera > (world.get ());

	tool->FollowObject (camera);

	auto const camera_rotation =
	    ::chai3d::cMatrix3d (9, 8, 7, 6, 5, 4, 3, 2, 1);

	camera->setLocalRot (camera_rotation);

	tool->SyncLocalRotation ();

	ASSERT_EQ (tool->getLocalRot ().str (), camera_rotation.str ());
}


#endif /* CHAI3D_TESTS_V1_TOOL_TESTBRIDGETTECHAI3DV1TOOL_HPP */
