// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Chai3D/BridgetteChai3D/include/Bridgette/Chai3D/v1/namespace.hpp
//
// Description: Bridgette Chai3D Integration Library namespace declaration.


#ifndef CHAI3D_BRIDGETTECHAI3D_INCLUDE_BRIDGETTE_CHAI3D_V1_NAMESPACE_HPP
#define CHAI3D_BRIDGETTECHAI3D_INCLUDE_BRIDGETTE_CHAI3D_V1_NAMESPACE_HPP


#define BRIDGETTE_CHAI3D_V1_OPEN_NAMESPACE                                     \
	namespace bridgette {                                                      \
		namespace chai3d {                                                     \
			inline namespace v1 {

#define BRIDGETTE_CHAI3D_V1_CLOSE_NAMESPACE                                    \
	}                                                                          \
	}                                                                          \
	}


#endif /* CHAI3D_BRIDGETTECHAI3D_INCLUDE_BRIDGETTE_CHAI3D_V1_NAMESPACE_HPP */
