// Copyright 2018 Bridgette Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
//
// File name:
// Bridgette/Chai3D/BridgetteChai3D/include/Bridgette/Chai3D/v1/ObjectFollower.hpp
//
// Description: Generic object follower class definition.


#ifndef CHAI3D_INCLUDE_BRIDGETTE_CHAI3D_V1_OBJECTFOLLOWER_HPP
#define CHAI3D_INCLUDE_BRIDGETTE_CHAI3D_V1_OBJECTFOLLOWER_HPP


#include <memory>
#include <type_traits>

#include <utk/idiom/TypeSmartPointer.hpp>

#include <world/CGenericObject.h>

#include "Bridgette/Chai3D/v1/namespace.hpp"


BRIDGETTE_CHAI3D_V1_OPEN_NAMESPACE


/**
   @brief A generic object follower

   @details The main purpose of this class is to simplify the creation of the
   objects that need to follow other objects. For example, a haptic tool
   following a camera. The use of this class allows to explicitly show the
   intent of making one object follow another and provides a set of convenient
   methods to control the follower object.

   @tparam FollowerType The type of the object that should follow another
                        object.

   @tparam LeaderType The type of the object that will be followed.
*/
template < class FollowerType, class LeaderType >
class ObjectFollower : public FollowerType {
	static_assert (
	    std::is_base_of< ::chai3d::cGenericObject, LeaderType >::value,
	    "The LeaderType should be derived from the chai3d::cGenericObject");

	static_assert (
	    std::is_base_of< ::chai3d::cGenericObject, FollowerType >::value,
	    "The FollowerType should be derived from the chai3d::cGenericObject");

public:
	using LeaderFollower = ObjectFollower< FollowerType, LeaderType >;

	using LeaderWeakPtr = std::weak_ptr< const LeaderType >;


	UTK_IDIOM_SHARED_PTR (LeaderFollower)
	UTK_IDIOM_UNIQUE_PTR (LeaderFollower)
	UTK_IDIOM_WEAK_PTR (LeaderFollower)

	UTK_IDIOM_MAKE_SHARED (LeaderFollower)
	UTK_IDIOM_MAKE_UNIQUE (LeaderFollower)


	using FollowerType::FollowerType;

	/**
	   @brief Sets this object to follow the given object

	   @param [in] i_object_to_follow A weak pointer to the object to follow.

	   @note The object cannot follow itself.
	*/
	void FollowObject (const LeaderWeakPtr i_object_to_follow) noexcept {
		followed_object_ = i_object_to_follow;
	}

	/**
	   @brief Checks if the object is following any object

	   @return Returns the state of following any object:

	           - true - the object is following some object;

	           - false - the object does not follow any object.
	*/
	bool IsFollowingAnyObject () const noexcept {
		return bool(followed_object_.lock ());
	}

	/**
	   @brief Checks if the object is following the given object

	   @param [in] i_object_to_check A pointer to an object to check if it is
	                                 followed by this object.

	   @return Returns the state of following the given object:

	           - true - the object is following the given object;

	           - false - the object does not follow the given object.
	*/
	bool
	    IsFollowingObject (const LeaderType* i_object_to_check) const noexcept {
		return i_object_to_check &&
		    (i_object_to_check == followed_object_.lock ().get ());
	}

	/**
	   @brief Synchronises the local rotation of this object with that of the
	   followed object if any
	*/
	void SyncLocalRotation () {
		auto const followed_object = followed_object_.lock ();

		if (followed_object) {
			/*
			  ::chai3d::cMatrix3d::equals() method takes non-constant reference
			  that makes it impossible to compare with constant object.
			*/
			auto followed_object_rotation{followed_object->getLocalRot ()};

			if (!getLocalRot ().equals (followed_object_rotation)) {
				setLocalRot (followed_object->getLocalRot ());
			}
		}
	}

	/**
	   @brief Stops following any object
	*/
	void StopFollowingObject () noexcept {
		followed_object_ = LeaderWeakPtr{};
	}


private:
	LeaderWeakPtr followed_object_;
};


BRIDGETTE_CHAI3D_V1_CLOSE_NAMESPACE


#endif /* CHAI3D_INCLUDE_BRIDGETTE_CHAI3D_V1_OBJECTFOLLOWER_HPP */
