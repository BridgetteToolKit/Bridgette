# Copyright 2018 Bridgette Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# File name: Bridgette/Chai3D/BridgetteChai3D/dependencies.cmake
#
# Description: Bridgette Chai3D Integration Library dependencies.


#####################
# Find dependencies #
#####################
function (BridgetteChai3D_find_dependencies)
  set (_options
    USE_FIND_DEPENDENCIES
    )
  set (_multi_value_args
    ""
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})


  if (i_USE_FIND_DEPENDENCIES)
    macro (find_function)
      include (CMakeFindDependencyMacro)

      find_dependency (${ARGN})
    endmacro (find_function)
  else ()
    macro (find_function)
      find_package (${ARGN})
    endmacro (find_function)
  endif ()


  find_function (CHAI3D 3.2 REQUIRED)

  # chai3d target does not provide proper values for the INTERFACE_* properties.
  set_property (
    TARGET chai3d
    APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES
    "${CHAI3D_INCLUDE_DIRS}"
    )
  set_property (
    TARGET chai3d
    APPEND PROPERTY INTERFACE_LINK_LIBRARIES
    "${CHAI3D_LIBRARIES}"
    )

  utk_cmake_find_or_download_package (
    PACKAGE utk_idiom
    FOLDER "Dependencies/utk"
    DOWNLOAD_AND_BUILD_BY_DEFAULT
    FIND_PACKAGE_OPTIONS
    0.2.1 REQUIRED
    DOWNLOAD_OPTIONS
    GIT_TAG v0.2.1
    GIT_REPOSITORY https://gitlab.com/UtilityToolKit/utk.idiom.git
    DOWNLOAD_OPTIONS_WITH_OVERRIDE
    GIT_TAG
    GIT_REPOSITORY
    )
endfunction (BridgetteChai3D_find_dependencies)


####################
# Use dependancies #
####################
function (BridgetteChai3D_use_dependencies)
  set (_options
    ""
    )
  set (_multi_value_args
    TARGET
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})

  if (NOT i_TARGET)
    message (SEND_ERROR "Provide TARGET argument")
  endif ()

  foreach (_target IN LISTS i_TARGET)
    target_link_libraries (
      ${_target}
      INTERFACE
      chai3d
      utk::idiom
      )
  endforeach (_target IN LISTS _interface_targets)
endfunction (BridgetteChai3D_use_dependencies)
