@PACKAGE_INIT@


##############################
# Find dependencies function #
##############################
function (BridgetteChai3D_find_dependencies)
  set (_options
    USE_FIND_DEPENDENCIES
    )
  set (_multi_value_args
    ""
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})


  if (i_USE_FIND_DEPENDENCIES)
    macro (find_function)
      include (CMakeFindDependencyMacro)

      find_dependency (${ARGN})
    endmacro (find_function)
  else ()
    macro (find_function)
      find_package (${ARGN})
    endmacro (find_function)
  endif ()


  find_function (CHAI3D 3.2 REQUIRED)

  # chai3d target does not provide proper values for the INTERFACE_* properties.
  set_property (
    TARGET chai3d
    APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES
    "${CHAI3D_INCLUDE_DIRS}"
    )
  set_property (
    TARGET chai3d
    APPEND PROPERTY INTERFACE_LINK_LIBRARIES
    "${CHAI3D_LIBRARIES}"
    )

  utk_cmake_find_or_download_package (
    PACKAGE utk_idiom
    FOLDER "Dependencies/utk"
    DOWNLOADED_TARGET "utk::idiom"
    DOWNLOAD_AND_BUILD_BY_DEFAULT
    IMPORTED_TARGET utk_idiom_target
    FIND_PACKAGE_TARGET "utk::idiom"
    FIND_PACKAGE_OPTIONS
    0.2.1 REQUIRED
    DOWNLOAD_OPTIONS
    GIT_TAG v0.2.1
    GIT_REPOSITORY https://gitlab.com/UtilityToolKit/utk.idiom.git
    DOWNLOAD_OPTIONS_WITH_OVERRIDE
    GIT_TAG
    GIT_REPOSITORY
    )
endfunction (BridgetteChai3D_find_dependencies)


#############################
# Use dependancies function #
#############################
function (BridgetteChai3D_use_dependencies)
  set (_options
    ""
    )
  set (_multi_value_args
    TARGET
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})

  if (NOT i_TARGET)
    message (SEND_ERROR "Provide TARGET argument")
  endif ()

  foreach (_target IN LISTS i_TARGET)
    target_link_libraries (
      ${_target}
      INTERFACE
      INTERFACE
      chai3d
      utk::idiom
      )
  endforeach (_target IN LISTS _interface_targets)
endfunction (BridgetteChai3D_use_dependencies)


#===============================================================================


#####################
# Find dependencies #
#####################
BridgetteChai3D_find_dependencies (USE_FIND_DEPENDENCIES)


# If the @TARGET_EXPORT_NAME@ target and the *-config-targets.cmake file does
# not exist then we are building the project and its tests, examples, etc. and
# running the configure for the first time. The *-config-targets.cmake file will
# only be generated during generate stage of project configuration, so we cannot
# use it.
if (NOT (TARGET @TARGET_EXPORT_NAME@) AND
    EXISTS "${CMAKE_CURRENT_LIST_DIR}/@TARGET_CMAKE_CONFIG_TARGETS_FILE_NAME@")
  include("${CMAKE_CURRENT_LIST_DIR}/@TARGET_CMAKE_CONFIG_TARGETS_FILE_NAME@")
endif ()


####################
# Use dependencies #
####################
# If the @TARGET_EXPORT_NAME@ is imported, then the CMake package is used by
# some external project, so it is required to setup interface dependencies.
get_target_property (@TARGET_EXPORT_NAME@_is_imported
  @TARGET_EXPORT_NAME@
  IMPORTED
  )

if (@TARGET_EXPORT_NAME@_is_imported)
  BridgetteChai3D_use_dependencies (TARGET @TARGET_EXPORT_NAME@)
endif ()
