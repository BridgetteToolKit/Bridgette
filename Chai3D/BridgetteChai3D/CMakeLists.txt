############################################################################
# Copyright 2018 Bridgette Tool Kit Open Source Contributors               #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#     http://www.apache.org/licenses/LICENSE-2.0                           #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
############################################################################

# File name: Chai3D/CMakeLists.txt
#
# Description: Bridgette Chai3D integration helper library project definition.


include (dependencies.cmake)


project (BridgetteChai3D
  LANGUAGES CXX
  VERSION   0.1.0
  )


######################################
# Project target and other variables #
######################################
set (${PROJECT_NAME}_TARGET_NAME ${PROJECT_NAME})
set (${PROJECT_NAME}_TARGET_EXPORT_NAME "Bridgette::Chai3D")


#####################
# Find dependencies #
#####################
BridgetteChai3D_find_dependencies ()


################################################
# Create project target and set its properties #
################################################
add_library (${${PROJECT_NAME}_TARGET_NAME} INTERFACE)
# Alias target is required to use as subproject
add_library (
  ${${PROJECT_NAME}_TARGET_EXPORT_NAME} ALIAS ${${PROJECT_NAME}_TARGET_NAME})

# Using target list for unification
set (${PROJECT_NAME}_TARGET_LIST
  "${${PROJECT_NAME}_TARGET_NAME}"
  )

# Export options
set_target_properties(${${PROJECT_NAME}_TARGET_NAME}
  PROPERTIES
  EXPORT_NAME "${${PROJECT_NAME}_TARGET_EXPORT_NAME}"
  )

# Versioning properties
set_target_properties (
  ${${PROJECT_NAME}_TARGET_NAME}
  PROPERTIES
  COMPATIBLE_INTERFACE_STRING  "${PROJECT_NAME}_VERSION"
  )

# utk.cmake specific options
set_target_properties (
  ${${PROJECT_NAME}_TARGET_NAME}
  PROPERTIES
  INTERFACE_UTK_CMAKE_INCLUDE_PREFIX         "Bridgette/Chai3D"
  INTERFACE_UTK_CMAKE_LANGUAGE               "CXX"
  INTERFACE_UTK_CMAKE_PROJECT_CXX_NAMESPACE  "bridgette;chai3d;inline;v1"
  )


####################
# Use dependencies #
####################
BridgetteChai3D_use_dependencies (TARGET ${${PROJECT_NAME}_TARGET_LIST})


#################
# Export header #
#################
utk_cmake_generate_export_header (
  TARGET            ${${PROJECT_NAME}_TARGET_LIST}
  COMMON_BASE_NAME  "${PROJECT_NAME}"
  )


##########################
# Versioning information #
##########################
utk_cmake_product_information (
  TARGET                    ${${PROJECT_NAME}_TARGET_LIST}
  COMMON_TARGET_IDENTIFIER  "${PROJECT_NAME}"
  CREATE_PRODUCT_INFO_FUNCTIONS
  NAME_STRING               "Bridgette Tool Kit Chai3D Integration Helper Library"
  CONTRIBUTORS_FILE         "${${PROJECT_NAME}_DIRECTORY}/CONTRIBUTORS"
  COPYRIGHT_HOLDER_STRING   "Bridgette Tool Kit Open Source Contributors"

# Commented because the library is just released and there is no year range for
# copyright info string.

#  COPYRIGHT_INFO_FILE "product_info/COPYRIGHT_INFO"
#  COPYRIGHT_INFO_UTF8_FILE "product_info/COPYRIGHT_INFO"
  LICENSE_FILE              "${PARENT_PROJECT_DIR}/LICENSE"
  LICENSE_NAME_STRING       "Apache License 2.0"
#  NOTICE_FILE               "NOTICE"  # There is no NOTICE file right now
  )

##########################################
# Include directories and link libraries #
##########################################
# The only place where the INTERFACE_INCLUDE_DIRECTORIES property is set. The
# utk_cmake_install does not do this because it does not provide a way to set
# properties for INTERFACE_LIBRARY targets.
utk_cmake_target_include_directories(
  TARGET  ${${PROJECT_NAME}_TARGET_LIST}
  INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
  INTERFACE
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
  INTERFACE
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>/include
  $<INSTALL_INTERFACE:include>
  )


#########################################################
# Subdirectories with headers, sources, resources, etc. #
#########################################################
add_subdirectory (include)


##########################################
# Install and export project and targets #
##########################################
include (GNUInstallDirs)

utk_cmake_install_project (
  TARGET           ${${PROJECT_NAME}_TARGET_NAME}
  INSTALL_DEVEL    ${${PROJECT_NAME}_INSTALL_DEVEL}
  INSTALL_SOURCES  ${${PROJECT_NAME}_INSTALL_DEVEL}

  CUSTOM_CMAKE_CONFIG_FILE  "${CMAKE_CURRENT_LIST_DIR}/cmake/cmake-target-config.cmake.in"

  CMAKE_CONFIG_FILE_OPTIONS
  INSTALL_DESTINATION  "${CMAKE_INSTALL_LIBDIR}/cmake/@TARGET_NAME@"

  CMAKE_CONFIG_VERSION_FILE_OPTIONS
  COMPATIBILITY  "ExactVersion" # For initial development, change to
                                # "SameMajorVersion" after the interface is
                                # stable.

  CMAKE_PACKAGE_FILE_OPTIONS
  COMPONENT    "Devel"
  DESTINATION  "${CMAKE_INSTALL_LIBDIR}/cmake/@TARGET_NAME@"
  )
